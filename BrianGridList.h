//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANGRIDLIST_H

#define BRIANGRIDLIST_H

#include <QtCore/QList>
#include <QtCore/QPair>

class BrianScopeWidget;

struct BrianGridline
{
	float value;
	unsigned intensity;
	unsigned lineposition;
};

class BrianGridList : public QList < BrianGridline >
{
	public:
		BrianGridList ( BrianScopeWidget & , unsigned , double , double , double , quint32 , bool , int , bool );

		void drawLines ( bool );

	private:
		quint32 doubleToBCSO ( double , double , quint32 , bool );
		quint32 incrementBCSO ( quint32 , quint32 , unsigned , bool );

		template < bool >
		void drawLines_private ( void );

		template < bool >
		inline void drawLine ( unsigned );

		BrianScopeWidget & scopewidget;
};

/** BrianInterval: A range that can determine if it overlaps with another range.
**/
class BrianInterval : public QPair < unsigned , unsigned >
{
	public:
		BrianInterval ( unsigned lower , unsigned upper ) : QPair < unsigned , unsigned > ( lower , upper )
		{
			Q_ASSERT ( lower <= upper );
		};

		// If two ranges try to occupy the same space, they are 'equal'.
		bool operator< ( const BrianInterval & other )
		{
			return ( this->upper () <= other.lower () );
		};

		bool operator> ( const BrianInterval & other )
		{
			return ( this->lower () >= other.upper () );
		};

		unsigned lower ( void ) const
		{
			return first;
		};

		unsigned upper ( void ) const
		{
			return second;
		};
};

/** BrianMultiRange: A list of non-overlapping sorted ranges
**/
class BrianMultiRange : public QList < BrianInterval >
{
	public:
		// Returns false if interval wasn't inserted because it
		// didn't fit.
		bool fitAndInsert ( BrianInterval other )
		{
			iterator i = qLowerBound ( begin () , end () , other );
			
			if ( i == end () || *i > other )
			{
				insert ( i , other );
				return true;
			}
			else
				return false;
		};
};

#endif /*BRIANGRIDLIST_H*/
