//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANCALLBACKS_H

#define BRIANCALLBACKS_H

#include <jack/jack.h>
#include "BrianJackConnection.h"

namespace BrianCallbacks
{
	int process ( jack_nframes_t , void * );
	int changeBlocksize ( jack_nframes_t , void * );
	void shutdown ( void * );
	int transport ( jack_transport_state_t , jack_position_t * , void * );
	void portConnect ( jack_port_id_t , jack_port_id_t , int , void * );
	int graphReorder ( void * );
}

#endif /* BRIANCALLBACKS_H */
