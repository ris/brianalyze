//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANJACKCONNECTION_H

#define BRIANJACKCONNECTION_H

#include <QtCore/QList>
#include <jack/jack.h>
#include <jack/transport.h>

#include "BrianDataManager.h"
#include "BrianCallbacks.h"
#include "BrianPointer.h"
#include "BrianDirtyRelayThread.h"

class BrianPortList : public QList < BrianPointer < BrianDataManager > > , public BrianPointerTarget {};

class BrianJackConnection : public QObject
{
	Q_OBJECT

	public:
		enum PaintThreading
		{
			None ,
			NoReduce ,
			Full
		};

		BrianJackConnection ( PaintThreading );
		~BrianJackConnection ( void );

		int jackProcess ( jack_nframes_t , void * );
		int jackChangeBlocksize ( jack_nframes_t , void * );
		void jackShutdown ( void * );
		void jackPortConnect ( void );
		void jackTransportStop ( void );

		void activate ( void );
		void deactivate ( void );

		unsigned getSamplerate ( void );

		BrianDataManager * addPort ( bool , bool );
		void removePort ( BrianDataManager * );

	private:
		jack_client_t * client;
		
		BrianPointer <BrianPortList> ports;
		bool samplehold;
		BrianDirtyRelayThread relaythread;

		volatile int in_process;

		const PaintThreading threaded_paint;

	signals:
		void newData ( void );
		void samplerateChanged ( unsigned );
		void blocksizeChanged ( unsigned );
		void portConnect ( void );
		void portListChanged ( void );
		void transportStop ( void );

	public slots:
		void paintAll ( void );
		void paintAllConnections ( QPainter & );
		void setLogFreq ( bool );
		void setLogAmp ( bool );
		void setSampleHold ( bool );
};

#endif /* BRIANJACKCONNECTION_H */
