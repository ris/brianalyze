//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianGridList.h"

#include "BrianMaths.h"
#include "BrianScopeWidget.h"

#include <cmath>

//
// Binary Coded Sub-Octals.
// 3 bits to represent each order of magnitude.
// M.S. 5 bits give sign and number of 'anchor' units.
// Each magnitude has a divisor determined by 'bases',
// which is also delivered in octal.
// Log ones - M.S. 4 bits hold loglevel, next 4 bits
// hold log sublevel. Rest as before.
//
BrianGridList::BrianGridList ( BrianScopeWidget & scopewidget_p , unsigned desired_p , double start_p , double end_p , double anchor_p , quint32 bases_p , bool log_p , int mapping_p , bool dimension_p ) : scopewidget ( scopewidget_p )
{
	quint32 start_bcso = incrementBCSO ( doubleToBCSO ( start_p , anchor_p , bases_p , log_p ) , bases_p , 0 , log_p );
	quint32 end_bcso = doubleToBCSO ( end_p , anchor_p , bases_p , log_p );

	double divisor[10];

	divisor[9] = anchor_p;

	// If we're doing a linear progression, we can precalculate the divisors
	// as they will be constant.
	if ( ! log_p )
		for ( int level = 8 ; level >= 0 ; level-- )
			divisor [ level ] = divisor [ level+1 ] / ( ( ( bases_p & ( 0x07 << ( level * 3 ) ) ) >> ( level * 3 ) ) + 1 );

	// Be sure that the first line will trigger these to change.
	unsigned intensity_maximum = 0;
	unsigned intensity_minimum = 9;

	for ( int level = 9 ; level >= 0 ; level-- )
	{
		if ( count () >= desired_p )
			break;

		quint32 current_bcso = start_bcso;

		// We want to snap UP to the first line at this level within the range.
		// If there is a partial below the current level, increment this level and blank out partial.
		if ( log_p && ( level == 9 ) )
		{
			if ( ( ((~0)<<28) & start_bcso ) != start_bcso )
				current_bcso = incrementBCSO ( start_bcso , bases_p , level , log_p ) & ((~0)<<28);
		}
		else if ( ( ( (~0) << ( level * 3 ) ) & start_bcso ) != start_bcso )
			current_bcso = incrementBCSO ( start_bcso , bases_p , level , log_p ) & ( (~0) << ( level * 3 ) );

		quint32 fieldmask;
		if ( log_p && ( level == 9 ) )
			fieldmask = (0x0f<<28);
		else if ( log_p && ( level == 8 ) )
			fieldmask = (0x0f<<24);
		else
			fieldmask = ( 0x07 << ( level * 3 ) );

		for (  ; current_bcso <= end_bcso ; current_bcso = incrementBCSO ( current_bcso , bases_p , level , log_p ) )
		{
			// Relevant level is a zero. Therefore already plotted by above level. Skip.
			if ( ! ( current_bcso & fieldmask ) )
				continue;
	
			// Work out what value this actually corresponds to.
			double value;
			if ( ! log_p )
			{
				value = ( -16.0 + ( ( current_bcso & (0x1f<<27) ) >> 27 ) ) * anchor_p;
				for ( int i = 8 ; i >= 0 ; i-- )
					value += ( ( current_bcso & ( 0x07 << ( i * 3 ) ) ) >> ( i * 3 ) ) * divisor[i];
			}
			else
			{
				value = exp10 ( ( current_bcso & (0x0f<<28) ) >> 28 );
				double ldivisor = value;

				value *= ( ( current_bcso & (0x0f<<24) ) >> 24 ) + 1;

				for ( int i = 7 ; i >= 0 ; i-- )
					value += ( ( current_bcso & ( 0x07 << ( i * 3 ) ) ) >> ( i * 3 ) ) * ( ldivisor /= ( ( bases_p & ( 0x07 << ( i * 3 ) ) ) >> ( i * 3 ) ) + 1 );
			}

			BrianGridline newgridline;
			newgridline.value = value;
			newgridline.intensity = level;

			intensity_minimum = qMin ( newgridline.intensity , intensity_minimum );
			intensity_maximum = qMax ( newgridline.intensity , intensity_maximum );

			//qDebug () << QString("Gridline bcso %1 value %2").arg(current_bcso,0,8).arg(newgridline.value);
			//qDebug () << QString("Gridline value %1 intensity %2").arg(newgridline.value).arg(newgridline.intensity);
			append ( newgridline );
		}
	}


	unsigned intensity_offset = intensity_minimum;
	float intensity_multiplier = 159.0 / ( intensity_maximum - intensity_minimum );

	QMutableListIterator<BrianGridline> i ( *this );

	float tempmatrix[16];
	glGetFloatv ( GL_PROJECTION_MATRIX , tempmatrix );

	//qDebug () << "Got GL_PROJECTION_MATRIX " << tempmatrix[0] << tempmatrix[1] << tempmatrix[2] << tempmatrix[3] << tempmatrix[4] << tempmatrix[5] << tempmatrix[6] << tempmatrix[7] << tempmatrix[8] << tempmatrix[9] << tempmatrix[10] << tempmatrix[11] << tempmatrix[12] << tempmatrix[13] << tempmatrix[14] << tempmatrix[15];

	float transform_scale;
	float transform_translate = 0.0;

	if ( ! dimension_p )
	{
		float t = scopewidget.width () * 0.5;
		transform_scale = tempmatrix[0] * t;
		transform_translate = ( tempmatrix[12] + 1.0 ) * t;
	}
	else
	{
		float t = scopewidget.height () * 0.5;
		transform_scale = tempmatrix[5] * -t;
		transform_translate = scopewidget.height () - ( ( tempmatrix[13] + 1.0 ) * t );
	}

	BrianMultiRange labelrange;

	while ( i.hasNext () )
	{
		BrianGridline & gridline = i.next();
		gridline.intensity = qMin ( (unsigned) ( ( gridline.intensity - intensity_offset ) * intensity_multiplier ) + 96 , (unsigned) 255 );
		
		switch ( mapping_p )
		{
			case -1:
				gridline.lineposition = (unsigned) ( ( BrianMaths::fromDecibels ( gridline.value ) * transform_scale ) + transform_translate );
				break;

			case 0:
				gridline.lineposition = (unsigned) ( ( gridline.value * transform_scale ) + transform_translate );
				break;

			case 1:
				gridline.lineposition = (unsigned) ( ( log2 ( gridline.value ) * transform_scale ) + transform_translate );
				break;

			default:
				qFatal ( "Unknown Mapping" );
				break;
		}
	}
}

quint32 BrianGridList::doubleToBCSO ( double target_p , double anchor_p , quint32 bases_p , bool log_p )
{
	if ( ! log_p )
	{
		double absolute_divisor = anchor_p;
	
		// M.S.bit is mid-way offset / sign. Allows +/- 16 toplevel units.
		double currently_represented = -16.0 * absolute_divisor;
	
		double p = floor ( ( target_p - currently_represented ) / absolute_divisor );
		currently_represented += p * absolute_divisor;
		quint32 bcso = (unsigned) p;
	
		if ( bcso > 0x1f )
			qFatal ( "Too big to represent" );
	
		for ( int level = 8 ; level >= 0 ; level-- )
		{
			bcso <<= 3;
			absolute_divisor /= ( ( bases_p & ( 0x07 << ( level * 3 ) ) ) >> ( level * 3 ) ) + 1;
		
			double partial = floor ( ( target_p - currently_represented ) / absolute_divisor );
			currently_represented += partial * absolute_divisor;
			bcso |= (unsigned) partial;
		}
	
		return bcso;
	}
	else
	{
		double loglevel = floor ( log10 ( target_p ) );

		if ( loglevel > 0x0f )
			qFatal ( "Too big to represent" );
	
		quint32 bcso = (unsigned) loglevel;
		double currently_represented = exp10 ( loglevel );
		bcso <<= 4;
	
		double absolute_divisor = currently_represented;
		double partial = floor ( ( target_p - currently_represented ) / absolute_divisor );
		currently_represented += partial * absolute_divisor;
		bcso |= (unsigned) partial;
	
		for ( int level = 7 ; level >= 0 ; level-- )
		{
			bcso <<= 3;
			absolute_divisor /= ( ( bases_p & ( 0x07 << ( level * 3 ) ) ) >> ( level * 3 ) ) + 1;
		
			double partial = floor ( ( target_p - currently_represented ) / absolute_divisor );
			currently_represented += partial * absolute_divisor;
			bcso |= (unsigned) partial;
		}
	
		return bcso;
	}
}

quint32 BrianGridList::incrementBCSO ( quint32 bcso_p , quint32 bases_p , unsigned level_p , bool log_p )
{
	if ( ! log_p )
	{
		quint32 result = bcso_p;
	
		quint32 bases = bases_p;
	
		for ( unsigned level = level_p ; level < 9 ; level++ )
		{
			if ( ( result & ( 0x07 << ( level * 3 ) ) ) == ( bases & ( 0x07 << ( level * 3 ) ) ) )
				result &= ~( 0x07 << ( level * 3 ) );
			else
				return result + ( 1 << ( level * 3 ) );
		}
	
		if ( ( result & ( 0x1f << 27 ) ) == ( 0x1f << 27 ) )
			qFatal ( "BCSO Overflow." );
	
		return result + (1<<27);
	}
	else
	{
		quint32 result = bcso_p;

		unsigned level = level_p;
	
		for ( ; level < 8 ; level++ )
		{
			if ( ( result & ( 0x07 << ( level * 3 ) ) ) == ( bases_p & ( 0x07 << ( level * 3 ) ) ) )
				result &= ~( 0x07 << ( level * 3 ) );
			else
				return result + ( 1 << ( level * 3 ) );
		}
	
		if ( level == 8 )
		{
			if ( ( result & (0x0f<<24) ) == (0x08<<24) )
				result &= ~(0x0f<<24);
			else
				return result + (1<<24);
		}
	
		if ( ( result & (0x0f<<28 ) ) == (0x0f<<28) )
			qFatal ( "BCSO Overflow." );
	
		// When switching magnitudes, we can't even be sure that the previous partial has
		// an equivalent representation in the new magnitude, so we'll just blank it for now.
		return ( result + (1<<28) ) & (0x0f<<28);
	}
}

void BrianGridList::drawLines ( bool dimension )
{
	glMatrixMode ( GL_MODELVIEW );
	glPushMatrix ();

		glLoadIdentity ();

		glMatrixMode ( GL_PROJECTION );
		glPushMatrix ();

			glLoadIdentity ();

			// Set up PROJECTION matrix for a 1to1 mapping to window coords.
			glOrtho ( 0.0 , scopewidget.width () , scopewidget.height () , 0.0 , -1.0 , 1.0 );

			glPushAttrib ( GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT | GL_LINE_BIT | GL_VIEWPORT_BIT );

				glViewport ( 0 , 0 , scopewidget.width () , scopewidget.height () );

				// When lines are drawn on top of each other, we want the most significant
				glBlendEquation ( GL_MAX );
		
				// Ugly artifacts with vertical lines
				glDisable ( GL_LINE_SMOOTH );

				if ( dimension )
					drawLines_private<true> ();
				else
					drawLines_private<false> ();

			glPopAttrib ();

		glMatrixMode ( GL_PROJECTION );
		glPopMatrix ();

	glMatrixMode ( GL_MODELVIEW );
	glPopMatrix ();
}

template < bool D >
void BrianGridList::drawLines_private ( void )
{
	for ( iterator i = begin () ; i != end () ; i++ )
	{
		// Set colour.
		glColor3ub ( (*i).intensity , (*i).intensity , (*i).intensity );

		// Draw line.
		drawLine<D> ( (*i).lineposition );
	}
}

template <>
inline void BrianGridList::drawLine<false> ( unsigned lineposition )
{
	glBegin ( GL_LINES );
		glVertex2i ( lineposition , 8 );
		glVertex2i ( lineposition , scopewidget.height () - 8 );
	glEnd ();
}

template <>
inline void BrianGridList::drawLine<true> ( unsigned lineposition )
{
	glBegin ( GL_LINES );
		glVertex2i ( 8 , lineposition );
		glVertex2i ( scopewidget.width () - 8 , lineposition );
	glEnd ();
}
