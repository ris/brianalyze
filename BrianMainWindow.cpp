//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianMainWindow.h"
#include "BrianGarbage.h"

#include "BrianPointer.h"

#include <sys/types.h>
#include <unistd.h>

#include <signal.h>

BrianMainWindow::BrianMainWindow ( QWidget * parent , BrianJackConnection::PaintThreading threaded_paint_p , bool use_stencil_p ) : QMainWindow ( parent ) ,
	garbagetimer ( this ) ,
	jackconnection ( threaded_paint_p )
{
	ui.setupUi ( this );

	setWindowTitle ( windowTitle () + " (" + QString::number ( getpid () ) + ')' );

	ui.freqcontrol->setSuffix ( tr ( "Hz" ) );
	ui.freqcontrol->setMargin ( 1.0 );
	ui.freqcontrol->setMinimum ( 0.0 );
	ui.freqcontrol->setMaximum ( jackconnection.getSamplerate () / 2 );
	ui.freqcontrol->setTickInterval ( 500.0 );
	ui.freqcontrol->reset ();

	ui.ampcontrol->multiplyMultiplier ( 100000.0 );
	ui.ampcontrol->setMargin ( 0.0001 );
	ui.ampcontrol->setMinimum ( 0.0 );
	ui.ampcontrol->setMaximum ( 1.0 );
	ui.ampcontrol->setTickInterval ( 0.01 );
	ui.ampcontrol->reset ();

	addPort ();
	addPort ();

	connect ( &jackconnection , SIGNAL(newData()) , ui.metawidget , SLOT(updateData()) , Qt::QueuedConnection );
	connect ( ui.metawidget , SIGNAL(paintSources()) , &jackconnection , SLOT(paintAll()) , Qt::DirectConnection );

	connect ( &jackconnection , SIGNAL(portConnect()) , ui.metawidget , SLOT(updateConnections()) );
	connect ( &jackconnection , SIGNAL(portListChanged()) , ui.metawidget , SLOT(updateConnections()) );
	connect ( ui.metawidget , SIGNAL(paintConnections(QPainter&)) , &jackconnection , SLOT(paintAllConnections(QPainter&)) , Qt::DirectConnection );

	connect ( &jackconnection , SIGNAL(transportStop()) , ui.holdbutton , SLOT(check()) , Qt::QueuedConnection );

	connect ( ui.freqcontrol , SIGNAL(lowChanged(float)) , ui.metawidget , SLOT(setLowFreq(float)) );
	connect ( ui.freqcontrol , SIGNAL(highChanged(float)) , ui.metawidget , SLOT(setHighFreq(float)) );
	connect ( ui.freqcontrol , SIGNAL(logChanged(bool)) , ui.metawidget , SLOT(setLogFreq(bool)) );
	connect ( ui.freqcontrol , SIGNAL(logChanged(bool)) , &jackconnection , SLOT(setLogFreq(bool)) );

	ui.metawidget->setLowFreq ( ui.freqcontrol->low () );
	ui.metawidget->setHighFreq ( ui.freqcontrol->high () );

	connect ( ui.ampcontrol , SIGNAL(lowChanged(float)) , ui.metawidget , SLOT(setLowAmp(float)) );
	connect ( ui.ampcontrol , SIGNAL(highChanged(float)) , ui.metawidget , SLOT(setHighAmp(float)) );
	connect ( ui.ampcontrol , SIGNAL(logChanged(bool)) , ui.metawidget , SLOT(setLogAmp(bool)) );
	connect ( ui.ampcontrol , SIGNAL(logChanged(bool)) , &jackconnection , SLOT(setLogAmp(bool)) );

	ui.metawidget->setLowAmp ( ui.ampcontrol->low () );
	ui.metawidget->setHighAmp ( ui.ampcontrol->high () );

	ui.metawidget->setUseStencil ( use_stencil_p );

	addbutton = new QToolButton ( );
	removebutton = new QToolButton ( );

	addbutton->setIcon ( QIcon ( ":/uiimages/images/plus.png" ) );
	removebutton->setIcon ( QIcon ( ":/uiimages/images/minus.png" ) );

	addbutton->setAutoRaise ( true );
	removebutton->setAutoRaise ( true );

	removebutton->setEnabled ( false );

	ui.tabwidget->setCornerWidget ( static_cast<QWidget *> ( addbutton ) , Qt::TopRightCorner );
	ui.tabwidget->setCornerWidget ( static_cast<QWidget *> ( removebutton ) , Qt::BottomLeftCorner );

	connect ( ui.tabwidget , SIGNAL(currentChanged(int)) , this , SLOT(disableEnableRemove(int)) );
	connect ( this , SIGNAL(tabChanged(int)) , this , SLOT(disableEnableRemove(int)) );

	connect ( addbutton , SIGNAL(clicked()) , this , SLOT(addPort()) );
	connect ( removebutton , SIGNAL(clicked()) , this , SLOT(removePort()) );

	connect ( &garbagetimer , SIGNAL(timeout()) , this , SLOT(garbageCall()) );
	garbagetimer.start ( 1000 );

	connect ( ui.holdbutton , SIGNAL(toggled(bool)) , &jackconnection , SLOT(setSampleHold(bool)) );

	jackconnection.activate ();
}

BrianMainWindow::~BrianMainWindow ( void )
{
	qDebug ( __PRETTY_FUNCTION__ );

	jackconnection.deactivate ();

	// Run a garbage collection to get anything that has already
	// lost all references.
	BrianGarbage::sweep ();
	// But we'll delete our own stuff from here on.
	garbagetimer.stop ();
}

void BrianMainWindow::addPort ( void )
{
	BrianDataManager * newmanager = jackconnection.addPort ( ui.freqcontrol->isLog () , ui.ampcontrol->isLog () );

	newmanager->setTabIndex ( ui.tabwidget->addTab ( newmanager , newmanager->getIcon () , newmanager->getPortName () ) );

	// set up connections
	connect ( newmanager , SIGNAL(iconChanged(unsigned,const QIcon&)) , this , SLOT(setTabIcon(unsigned,const QIcon&)) );
	connect ( newmanager , SIGNAL(nameChanged(unsigned,const QByteArray&)) , this , SLOT(setTabText(unsigned,const QByteArray&)) );
	connect ( newmanager , SIGNAL(iconChanged(unsigned,const QIcon&)) , ui.metawidget , SLOT(updateConnections()) );
	connect ( newmanager , SIGNAL(nameChanged(unsigned,const QByteArray&)) , ui.metawidget , SLOT(updateConnections()) );

	emit tabChanged ( ui.tabwidget->currentIndex () );
}

void BrianMainWindow::removePort ( void )
{
	BrianDataManager * manager = static_cast<BrianDataManager*> ( ui.tabwidget->widget ( ui.tabwidget->currentIndex () ) );

	ui.tabwidget->removeTab ( ui.tabwidget->currentIndex () );

	jackconnection.removePort ( manager );
}

void BrianMainWindow::setTabIcon ( unsigned n , const QIcon & icon )
{
	ui.tabwidget->setTabIcon ( n , icon );
}

void BrianMainWindow::setTabText ( unsigned n , const QByteArray & text )
{
	ui.tabwidget->setTabText ( n , text );
}

void BrianMainWindow::disableEnableRemove ( int current )
{
	removebutton->setEnabled ( ui.tabwidget->widget ( current )->inherits ( "BrianDataManager" ) && ( ui.tabwidget->count () > 3 ) );
}

void BrianMainWindow::garbageCall ( void )
{
	BrianGarbage::sweep ();
}

QSet < BrianPortList * > BrianGarbage::portlist_set;
QSet < BrianDataManager * > BrianGarbage::datamanager_set;
QSet < BrianDataPainter * > BrianGarbage::datapainter_set;
QSet < BrianDataSupplier * > BrianGarbage::datasupplier_set;
QSet < BrianTransfer * > BrianGarbage::transfer_set;

void BrianGarbage::add ( BrianPortList * object_p )
{
	portlist_set.insert ( object_p );
}

void BrianGarbage::add ( BrianDataManager * object_p )
{
	datamanager_set.insert ( object_p );
}

void BrianGarbage::add ( BrianDataPainter * object_p )
{
	datapainter_set.insert ( object_p );
}

void BrianGarbage::add ( BrianDataSupplier * object_p )
{
	datasupplier_set.insert ( object_p );
}

void BrianGarbage::add ( BrianTransfer * object_p )
{
	transfer_set.insert ( object_p );
}

void BrianGarbage::sweep ( void )
{
	// Order is semi-important in that if you don't use the correct order, it will take
	// several passes for dependency disappearance to propogate, meaning objects will be
	// around for longer thean they need to be.

	//qDebug () << "Begin Sweep:";

	QSet< BrianPortList * >::iterator portlist_i = BrianGarbage::portlist_set.begin ();
	while ( portlist_i != BrianGarbage::portlist_set.end () )
	{
		//qDebug () << "BrianPortList" << *portlist_i << "with" << (*portlist_i)->referrers << "refs.";
		if ( ! (*portlist_i)->referrers )
		{
			delete *portlist_i;
			portlist_i = portlist_set.erase ( portlist_i );
			qDebug () << "Sweep removed BrianPortList";
		}
		else
			portlist_i++;
	}

	QSet< BrianDataManager * >::iterator datamanager_i = BrianGarbage::datamanager_set.begin ();
	while ( datamanager_i != BrianGarbage::datamanager_set.end () )
	{
		//qDebug () << "BrianDataManager" << *datamanager_i << "with" << (*datamanager_i)->referrers << "refs.";
		if ( ! (*datamanager_i)->referrers )
		{
			delete *datamanager_i;
			datamanager_i = datamanager_set.erase ( datamanager_i );
			qDebug () << "Sweep removed BrianDataManager";
		}
		else
			datamanager_i++;
	}

	QSet< BrianDataSupplier * >::iterator datasupplier_i = BrianGarbage::datasupplier_set.begin ();
	while ( datasupplier_i != BrianGarbage::datasupplier_set.end () )
	{
		//qDebug () << "BrianDataSupplier" << *datasupplier_i << "with" << (*datasupplier_i)->referrers << "refs.";
		if ( ! (*datasupplier_i)->referrers )
		{
			delete *datasupplier_i;
			datasupplier_i = datasupplier_set.erase ( datasupplier_i );
			qDebug () << "Sweep removed BrianDataSupplier";
		}
		else
			datasupplier_i++;
	}

	QSet< BrianTransfer * >::iterator transfer_i = BrianGarbage::transfer_set.begin ();
	while ( transfer_i != BrianGarbage::transfer_set.end () )
	{
		//qDebug () << "BrianTransfer" << *transfer_i << "with" << (*transfer_i)->referrers << "refs.";
		if ( ! (*transfer_i)->referrers )
		{
			delete *transfer_i;
			transfer_i = transfer_set.erase ( transfer_i );
			qDebug () << "Sweep removed BrianTransfer";
		}
		else
			transfer_i++;
	}

	QSet< BrianDataPainter * >::iterator datapainter_i = BrianGarbage::datapainter_set.begin ();
	while ( datapainter_i != BrianGarbage::datapainter_set.end () )
	{
		//qDebug () << "BrianDataPainter" << *datapainter_i << "with" << (*datapainter_i)->referrers << "refs.";
		if ( ! (*datapainter_i)->referrers )
		{
			delete *datapainter_i;
			datapainter_i = datapainter_set.erase ( datapainter_i );
			qDebug () << "Sweep removed BrianDataPainter";
		}
		else
			datapainter_i++;
	}
}

#include "BrianMainWindow.moc"
