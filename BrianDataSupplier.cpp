//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianDataSupplier.h"

#include <QtCore/QtDebug>
#include <fftw3.h>

#include "BrianJackConnection.h"
#include "BrianGarbage.h"

BrianDataSupplier::BrianDataSupplier ( unsigned samplerate_p , unsigned blocksize_p , unsigned analysis_periods_p , BrianDataPainter * painter_p ) :
	samplerate ( samplerate_p ) ,
	blocksize ( blocksize_p ) ,
	analysis_periods ( analysis_periods_p )
{
	Q_ASSERT ( blocksize );
	Q_ASSERT ( analysis_periods );

	transfer[0].unsafeSet ( new BrianTransfer ( ( blocksize * analysis_periods ) + 2 , painter_p ) );
	transfer[1].unsafeSet ( new BrianTransfer ( ( blocksize * analysis_periods ) + 2 , painter_p ) );
	transfer[2].unsafeSet ( new BrianTransfer ( ( blocksize * analysis_periods ) + 2 , painter_p ) );

	BrianGarbage::add ( transfer[0].pointer () );
	BrianGarbage::add ( transfer[1].pointer () );
	BrianGarbage::add ( transfer[2].pointer () );

	persistbuffer = (float *) fftwf_malloc ( blocksize * analysis_periods * sizeof(float) );

	// TODO consider half-empty newish buffers introducing artifacts?
	bzero ( persistbuffer , sizeof(float) * blocksize * analysis_periods );

	//qDebug () << "Built Supplier" << this << "with s =" << analysis_periods * blocksize;
}

BrianDataSupplier::~BrianDataSupplier ( void )
{
	fftwf_free ( persistbuffer );

	//qDebug () << "Deleted Supplier" << this << "with s =" << analysis_periods * blocksize;
}

BrianTransfer * BrianDataSupplier::process ( float * in , BrianTransfer * latest_complete , BrianTransfer * reading_from )
{
	// Shunt data along on buffer.
	// No, this doesn't use a ringbuffer. We're interested in the latest data only.
	// Getting behind on data is worse than missing a block or two.
	// Doing that in a lockfree ringbuffer would be complex and probably end up
	// needing just as much memmove/memcpying.

	// Shunt back existing entries on buffer
	memmove ( persistbuffer , persistbuffer + blocksize , sizeof(float) * blocksize * ( analysis_periods - 1 ) );

	// Put new data on end of buffer
	memcpy ( persistbuffer + ( blocksize * ( analysis_periods - 1 ) ) , in , sizeof(float) * blocksize );

	// Find a place to do the transfer.
	//
	// This scheme assumes two things:
	// a: aligned pointer reads & writes are atomic on this arch.
	// b: the compiler aligns BrianDataManager::reading_from and latest_complete.
	//
	// latest_complete is under this thread's control, so there shouldn't be a race.
	// reading_from can only change to latest_complete, which we won't use anyway.
	unsigned i = 0;
	while ( transfer[i] == latest_complete || transfer[i] == reading_from )
		i = ( i + 1 ) % 3;

	//qDebug () << "latest_complete =" << latest_complete << "reading_from =" << reading_from << "wrote to" << transfer[i].pointer () << ( latest_complete == reading_from );

	memcpy ( transfer[i]->getData () , persistbuffer , sizeof(float) * ( ( blocksize * analysis_periods ) + 2 ) );
	transfer[i]->setSamplerate ( samplerate );

	return transfer[i].pointer ();
}

void BrianDataSupplier::changeSamplerate ( unsigned newrate )
{
	samplerate = newrate;
}
