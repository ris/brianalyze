//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANSCOPEWIDGET_H

#define BRIANSCOPEWIDGET_H

#include <QtOpenGL>
#include <QGLWidget>
#include <QtCore/QList>
#include <QtGui/QFont>

#include "BrianGridList.h"

#include <signal.h>

class BrianScopeWidget : public QGLWidget
{
	Q_OBJECT

	public:
		BrianScopeWidget ( QWidget * = 0 );
		~BrianScopeWidget ( void );

		bool hasShaders ( void ) const
		{
			return has_shaders;
		};

		void setGridLists ( BrianGridList **, BrianGridList **);

		void setLowFreq ( float );
		void setHighFreq ( float );

		void setLogFreq ( bool );

		void setLowAmp ( float );
		void setHighAmp ( float );

		void setLogAmp ( bool );

		void setUseStencil ( bool );

		void updateData ( void );

	signals:
		void paintSources ( void );

	private:
		void initializeGL ( void );
		void resizeGL ( int , int );
		void paintGL ( void );

		void setupProjectionMatrix ( void );
		void constructStencil ( void );

		void hideEvent ( QHideEvent * );
		void showEvent ( QShowEvent * );

		void generateGrid ( void );

		bool samplehold;
		bool has_shaders;
		bool has_vbo;
		bool log_frequency;
		bool log_value;

		bool use_stencil;

		float low_freq , high_freq;
		float low_val , high_val;

		BrianGridList ** freqgridlist;
		BrianGridList ** ampgridlist;

		static const quint8 stipplepattern[128];
};

#endif /* BRIANSCOPEWIDGET_H */
