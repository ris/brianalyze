//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANFREQSPINBOX_H

#define BRIANFREQSPINBOX_H

#include <QtGui/QDoubleSpinBox>

class BrianFreqSpinBox : public QDoubleSpinBox
{
	Q_OBJECT

	public:
		BrianFreqSpinBox ( QWidget * = 0 );

	signals:
		void valueChanged ( float );
//		void valueChanged ( int );

		void minimumChanged ( int );
		void maximumChanged ( int );

	public slots:
//		void setValue ( int );
		void setMinimum_slot ( float );
		void setMaximum_slot ( float );

	private slots:
		void valueChangedRelay ( double );

	private:
		//double valueFromText ( const QString & ) const;
		//QString textFromValue ( double value ) const;
		//QValidator::State validate ( QString & input, int & pos ) const;

		// I can't be the only person that thinks qregexp's const behaviour is really badly designed.
		//mutable QRegExp value_cap;
};

#endif /* BRIANFREQSPINBOX_H */
