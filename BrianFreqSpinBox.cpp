//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianFreqSpinBox.h"

#include <QtGui/QLineEdit>

BrianFreqSpinBox::BrianFreqSpinBox ( QWidget * parent ) : QDoubleSpinBox ( parent ) //,
//	value_cap ( "^(((\\d*\\.|\\d)\\d*)\\s*([a-z]*))?$" )
{
//	value_cap.setCaseSensitivity ( Qt::CaseInsensitive );

	// Sane Default
	setMaximum ( 40000.0 );
	setDecimals ( 1 );
	setAlignment ( Qt::AlignRight );
	setKeyboardTracking ( false );
//	setSuffix ( tr ( "Hz" ) );

	connect ( this , SIGNAL(valueChanged(double)) , this , SLOT(valueChangedRelay(double)) );
}

//
// !!! All the actual useful stuff yanked until I can get it to feel right. !!!
//

// double BrianFreqSpinBox::valueFromText ( const QString & text ) const
// {
// 	value_cap.indexIn ( text );
// 	double value = value_cap.cap( 2 ).toDouble ();
// 
// 	if ( ! value_cap.cap( 4 ).compare ( "khz" , Qt::CaseInsensitive ) )
// 		value *= 1000.0;
// 
// 	return value;
// }

// QString BrianFreqSpinBox::textFromValue ( double value ) const
// {
// 	return QString::number( value , 'f' , decimals () ).append ( tr ( "Hz" ) );
// }

// QValidator::State BrianFreqSpinBox::validate ( QString & input, int & pos ) const
// {
// 	if ( value_cap.exactMatch ( input ) )
// 		return QValidator::Acceptable;
// 	else
// 		return QValidator::Invalid;
// }

void BrianFreqSpinBox::valueChangedRelay ( double value )
{
	emit valueChanged ( (float) value );
}

//void BrianFreqSpinBox::setValue ( int value )
//{
//	QDoubleSpinBox::setValue ( (float) value );
//}

void BrianFreqSpinBox::setMinimum_slot ( float newminimum )
{
	if ( (int) newminimum != minimum () )
		emit minimumChanged ( (int) newminimum );

	setMinimum ( newminimum );
}

void BrianFreqSpinBox::setMaximum_slot ( float newmaximum )
{
	if ( (int) newmaximum != maximum () )
		emit maximumChanged ( (int) newmaximum );

	setMaximum ( newmaximum );
}

#include "BrianFreqSpinBox.moc"
