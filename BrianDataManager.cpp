//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianDataManager.h"

#include "BrianJackConnection.h"
#include "BrianDataPainter.h"
#include "BrianGarbage.h"

#include <QtCore/QDebug>
#include <QtGui/QColorDialog>
#include <QtGui/QPainter>

BrianDataManager::BrianDataManager ( jack_client_t * client_p , unsigned analysisperiods_p , enum BrianWindowType windowtype_p , bool log_freq_p , bool log_val_p , QColor colour_p , const QByteArray & name_p ) :
	QWidget ( 0 ) ,
	latest_complete ( 0 ) ,
	reading_from ( 0 ) ,
	jackclient ( client_p ) ,
	samplerate ( jack_get_sample_rate ( jackclient ) ) ,
	blocksize ( jack_get_buffer_size ( jackclient ) ) ,
	analysisperiods ( analysisperiods_p ) ,
	windowtype ( windowtype_p ) ,
	log_freq ( log_freq_p ) ,
	log_val ( log_val_p ) ,
	colour ( colour_p ) ,
	portname ( name_p )
{
	datapainter.unsafeSet ( new BrianDataPainter ( blocksize * analysisperiods , windowtype , log_freq , log_val , colour ) );
	// Depends on value of datapainter so must be initialized here.
	datasupplier.unsafeSet ( new BrianDataSupplier ( samplerate , blocksize , analysisperiods , datapainter.pointer () ) );

	BrianGarbage::add ( datapainter.pointer () );
	BrianGarbage::add ( datasupplier.pointer () );

	Q_ASSERT ( analysisperiods );

	if ( ( jackport = jack_port_register ( jackclient , portname , JACK_DEFAULT_AUDIO_TYPE , JackPortIsInput | JackPortIsTerminal , 0 ) ) == NULL )
		qFatal ( "Couldn't register port." );

	ui.setupUi ( this );

	ui.blocksbox->setValue ( analysisperiods );
	ui.namebox->setText ( portname );
	ui.windowfunctioncombo->setCurrentIndex ( windowtype );
	setUiColour ();

	connect ( ui.blocksbox , SIGNAL(valueChanged(int)) , this , SLOT(changeAnalysisPeriods(int)) );
	connect ( ui.colourbutton , SIGNAL(clicked()) , this , SLOT(colourClicked()) );
	connect ( ui.namebox , SIGNAL(editingFinished()) , this , SLOT(changePortName()) );
	connect ( ui.windowfunctioncombo , SIGNAL(currentIndexChanged(int)) , this , SLOT(changeWindowType(int)) );
}

BrianDataManager::~BrianDataManager ( )
{
	qDebug ( __PRETTY_FUNCTION__ );

	// unregister port
	jack_port_unregister ( jackclient , jackport );
}

void BrianDataManager::setUiColour ( void )
{
	QPixmap pixmap ( 16 , 16 );
	pixmap.fill ( Qt::black );

	QPainter ( &pixmap ).fillRect ( 1 , 1 , 14 , 14 , colour );

	colouricon = QIcon ( pixmap );

	ui.colourbutton->setIcon ( colouricon );

	// Change tab icon.
	emit iconChanged ( tabindex , colouricon );
}

void BrianDataManager::colourClicked ( void )
{
	const QColor & newcolour ( QColorDialog::getColor ( colour ) );

	if ( newcolour.isValid () )
		changeColour ( newcolour );
};

void BrianDataManager::process ( bool samplehold_p )
{
	BrianPointer<BrianDataSupplier> datasupplier_snapshot ( datasupplier );

	BrianPointer<BrianTransfer> transfer ( datasupplier_snapshot->process ( (float *) jack_port_get_buffer ( jackport , blocksize ) , latest_complete.pointer () , reading_from.pointer () ) );

	if ( ! samplehold_p )
		latest_complete = transfer;

	//qDebug () << "Wrote" << latest_complete;
}

void BrianDataManager::prepare ( void )
{
	// We might not have any data for this port yet. If so, skip.
	// FIXME hack?
	if ( latest_complete )
	{
		reading_from = latest_complete;
		//qDebug () << "Read " << reading_from;

		reading_from->prepare ();

		// TODO prevent unsuitable painters (log, non-log etc.) painting to scopewidget.

		// Not actually necessary and allows us to tell
		// when we're getting new data because we'll be pointed
		// to a new transfer.
		//reading_from = 0;
	}
}

void BrianDataManager::reduce ( void )
{
	// Again, we may not have data yet.
	if ( reading_from )
		// Nothing important should have happened since prepare () was called
		reading_from->reduce ();
}

bool BrianDataManager::paintConnections ( QPainter & painter_p )
{
	QByteArray text ( portname + ": " );

	const char ** connections = jack_port_get_connections ( jackport );

	if ( connections )
	{
		text += connections[0];

		for ( unsigned i = 1 ; connections[i] != 0 ; i++ )
		{
			if ( painter_p.fontMetrics().width ( text ) >= 200 )
			{
				text += ", ...";
				break;
			}

			text += ", ";
			text += connections[i];
		}

		free ( connections );
	}

	if ( painter_p.window ().contains ( painter_p.combinedTransform().mapRect ( painter_p.fontMetrics().boundingRect ( text ) ) ) )
	{
		painter_p.setPen ( colour );
		painter_p.drawText ( 0 , 0 , text );
		return true;
	}
	else
		return false;
}

//
// !!! Always acquire supplier locks first to prevent deadlocks. !!!
//

void BrianDataManager::changeSamplerate ( unsigned newrate )
{
	// FIXME

// 	// Trivial, but we still need to lock the supplier_rebuild.
// 	QMutexLocker supplier_locker ( & supplier_rebuild );
// 
// 	samplerate = newrate;
// 
// 	datasupplier->changeSamplerate ( newrate );
}

void BrianDataManager::changeBlocksize ( unsigned newsize )
{
	// FIXME

// 	QMutexLocker supplier_locker ( & supplier_rebuild );
// 	QMutexLocker painter_locker ( & painter_rebuild );
// 
// 	blocksize = newsize;
// 
// 	// rebuild painter
// 	datapainter = new BrianDataPainter ( analysisperiods * blocksize , windowtype , log_freq , log_val , colour );
// 
// 	// rebuild supplier
// 	datasupplier = new BrianDataSupplier ( samplerate , blocksize , analysisperiods , datapainter );
}

void BrianDataManager::changeAnalysisPeriods ( int newperiods )
{
	Q_ASSERT( newperiods > 0 );

	QMutexLocker supplier_locker ( & supplier_rebuild );
	QMutexLocker painter_locker ( & painter_rebuild );

	analysisperiods = newperiods;

	// rebuild painter
	datapainter = new BrianDataPainter ( analysisperiods * blocksize , windowtype , log_freq , log_val , colour );
	
	// rebuild supplier
	datasupplier = BrianPointer<BrianDataSupplier> ( new BrianDataSupplier ( samplerate , blocksize , analysisperiods , datapainter.pointer () ) );

	BrianGarbage::add ( datapainter.pointer () );
	BrianGarbage::add ( datasupplier.pointer () );
}

//
// Just make sure the enum is in the same order as the combobox
//
void BrianDataManager::changeWindowType ( int windowtype_p )
{
	QMutexLocker painter_locker ( & painter_rebuild );

	windowtype = (enum BrianWindowType) windowtype_p;

	datapainter->setWindowType ( windowtype );
}

void BrianDataManager::changePortName ( void )
{
	QByteArray newname = ui.namebox->text ().toLatin1 ();

	if ( jack_port_set_name ( jackport , newname ) )
	{
		ui.namebox->setText ( portname );
		return;
	}

	portname = newname;
	emit nameChanged ( tabindex , portname );
}

void BrianDataManager::changeLogFrequency ( bool logfreq_p )
{
	QMutexLocker painter_locker ( & painter_rebuild );

	log_freq = logfreq_p;

	datapainter->setLogFrequency ( log_freq );
}

void BrianDataManager::changeLogValue ( bool logval_p )
{
	QMutexLocker painter_locker ( & painter_rebuild );

	log_val = logval_p;

	datapainter->setLogValue ( log_val );
}

void BrianDataManager::changeColour ( const QColor & newcolour )
{
	QMutexLocker painter_locker ( & painter_rebuild );

	colour = newcolour;
	setUiColour ();

	datapainter->changeColour ( newcolour );
}

const QByteArray & BrianDataManager::getPortName ( void ) const
{
	return portname;
}

const QIcon & BrianDataManager::getIcon ( void ) const
{
	return colouricon;
}

const QColor & BrianDataManager::getColour ( void ) const
{
	return colour;
}

void BrianDataManager::setTabIndex ( unsigned i )
{
	tabindex = i;
	emit nameChanged ( tabindex , portname );
}

int BrianDataManager::tabIndex ( void ) const
{
	return tabindex;
}

#include "BrianDataManager.moc"
