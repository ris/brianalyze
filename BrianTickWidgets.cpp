//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianTickWidgets.h"

#include <QtGui/QPainter>

BrianAbstractTickWidget::BrianAbstractTickWidget ( QWidget * parent_p ) : QWidget ( parent_p ) ,
	gridlist ( 0 )
{
	setAutoFillBackground ( true );
}

void BrianAbstractTickWidget::setGridList ( const BrianGridList ** gridlist_p )
{
	gridlist = gridlist_p;
}

BrianHTickWidget::BrianHTickWidget ( QWidget * parent_p ) : BrianAbstractTickWidget ( parent_p )
{}

BrianVTickWidget::BrianVTickWidget ( QWidget * parent_p ) : BrianAbstractTickWidget ( parent_p )
{}

void BrianHTickWidget::paintEvent ( QPaintEvent * event )
{
	QPainter painter ( this );
	painter.setRenderHint ( QPainter::TextAntialiasing , false );

	BrianMultiRange labelrange;

	if ( gridlist && *gridlist )
	{
		//painter.setFont ( *font );

		foreach ( BrianGridline gridline , **gridlist )
		{
			QString labeltext = QString::number ( gridline.value );

			int width = fontMetrics().width ( labeltext );

			QPoint textposition ( 56 + gridline.lineposition - ( width / 2 ) , 16 + ( fontMetrics().height () / 2 ) );

			if ( labelrange.fitAndInsert ( BrianInterval ( textposition.x () , textposition.x () + width ) ) )
			{
				painter.setPen ( QColor ( gridline.intensity , gridline.intensity , gridline.intensity ) );
				painter.drawText ( textposition , labeltext );
			}
		}

		painter.setPen ( Qt::white );
		QString legendtext ( tr ( "Frequency / Hz" ) );
		unsigned legendwidth = fontMetrics().width ( legendtext );
		painter.drawText ( QPoint ( ( width () - legendwidth ) / 2 , 42 ) , legendtext );
	}
}

void BrianVTickWidget::paintEvent ( QPaintEvent * event )
{
	QPainter painter ( this );
	painter.setRenderHint ( QPainter::TextAntialiasing , false );

	BrianMultiRange labelrange;

	if ( gridlist && *gridlist )
	{
		//painter.setFont ( *font );

		foreach ( BrianGridline gridline , **gridlist )
		{
			QString labeltext = QString::number ( gridline.value );

			int width = fontMetrics().width ( labeltext );
			int height = fontMetrics().height ();

			QPoint textposition ( 40 - ( width / 2 ) , gridline.lineposition + ( height / 2 ) );

			if ( labelrange.fitAndInsert ( BrianInterval ( textposition.y () - height , textposition.y () ) ) )
			{
				painter.setPen ( QColor ( gridline.intensity , gridline.intensity , gridline.intensity ) );
				painter.drawText ( textposition , labeltext );
			}
		}

		painter.setPen ( Qt::white );
		QString legendtext ( tr ( "dBFS" ) );
		unsigned legendheight = fontMetrics().height ();
		painter.drawText ( QPoint ( 2 , ( height () + legendheight ) / 2 ) , legendtext );
	}
}

#include "BrianTickWidgets.moc"
