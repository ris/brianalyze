//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANWINDOW_H

#define BRIANWINDOW_H

#include <cmath>

enum BrianWindowType
{
	None ,
	Triangle ,
	Hann ,
	Hamming ,
	FlatTop ,
	Sine
};

namespace BrianWindow
{
	template < class D >
	class Base
	{
		public:
			static void generate ( float * buffer , unsigned n )
			{
				float inverse_n = 1.0 / n;
			
				for ( unsigned i = 0 ; i < n ; i++ )
					buffer[i] = D::definition ( i * inverse_n ) * D::factor;
			};
	};

	class Triangle : public Base < Triangle >
	{
		public:
			static inline float definition ( float n )
			{
				return 1.0 - fabs ( 1.0 - ( n * 2.0 ) ) ;
			};

			static const float factor = 1.0;
	};

	class Hann : public Base < Hann >
	{
		public:
			static inline float definition ( float n )
			{
				return 0.5 - ( 0.5 * cos ( 2.0 * (float) M_PI * n ) );
			};

			static const float factor = 2.0;
	};

	class Hamming : public Base < Hamming >
	{
		public:
			static inline float definition ( float n )
			{
				return 0.53836 - ( 0.46164 * cos ( 2.0 * (float) M_PI * n ) );
			};

			static const float factor = 1.8574931;
	};

	class FlatTop : public Base < FlatTop >
	{
		public:
			static inline float definition ( float n )
			{
				return 1.0 - ( 1.93 * cos ( 2.0 * (float) M_PI * n ) ) + ( 1.29 * cos ( (float) M_PI * 4.0 * n ) ) - ( 0.388 * cos ((float)  M_PI * 6.0 * n ) ) + ( 0.032 * cos ( (float) M_PI * 8.0 * n ) );
			};

			//FIXME
			static const float factor = 1.0;
	};

	class Sine : public Base < Sine >
	{
		public:
			static inline float definition ( float n )
			{
				return sin ( M_PI * n );
			};

			static const float factor = 2.0;
	};
};

#endif /* BRIANWINDOW_H */
