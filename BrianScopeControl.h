//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANSCOPECONTROL_H

#define BRIANSCOPECONTROL_H

#include "ui_scopecontrol.h"

class BrianScopeControl : public QWidget
{
	Q_OBJECT

	public:
		BrianScopeControl ( QWidget * = 0 );

		bool isLog ( void ) const;

		float low ( void ) const;
		float high ( void ) const;

	signals:
		void lowChanged ( float );
		void highChanged ( float );
		void logChanged ( bool );

	public slots:
		void setMinimum ( float );
		void setMaximum ( float );
		void setSuffix ( const QString & );
		void setTickInterval ( float );
		void setMargin ( float );
		void reset ( void );
		void multiplyMultiplier ( float );

	private slots:
		void lowChanged_private ( float );
		void highChanged_private ( float );
		void lowSpinboxChanged ( double );
		void highSpinboxChanged ( double );
		void logDisallowZero ( bool );

	private:
		Ui_BrianScopeControl ui;
		float margin;
		float realmin;
		bool realminswappedout;
};

#endif /* BRIANSCOPECONTROL_H */
