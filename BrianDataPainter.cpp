//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianDataPainter.h"
#include "BrianMaths.h"

#include <cmath>

// Quick way of getting gl headers
#include <QGLWidget>

#include <QtCore/QDebug>

#include <signal.h>

BrianDataPainter::BrianDataPainter ( unsigned samples_p , enum BrianWindowType windowtype_p , bool log_frequency_p , bool log_value_p , const QColor & colour_p ) :
	samples ( samples_p ) ,
	windowtype ( windowtype_p ) ,
	log_frequency ( log_frequency_p ) ,
	log_value ( log_value_p ) ,
	logtable ( 0 ) ,
	window ( 0 ) ,
	colour ( colour_p ) ,
	last_data ( 0 ) ,
	last_log_frequency ( false ) ,
	last_log_value ( false ) ,
	last_windowtype ( None )
{
	// Allocate ping-pong buffers.
	// Slight overestimate but easy bitrithmatic.
	unsigned bufferlength = ( ( samples + 2 ) + 0x07 ) & ~0x07;
	buffer[0] = (float *) fftwf_malloc ( bufferlength * sizeof(float) );
	buffer[1] = (float *) fftwf_malloc ( bufferlength * sizeof(float) );

	if ( log_frequency )
	{
		// We're going to need a log table. Logs are far more expensive than
		// memory bandwidth. On my machine at least.
		logtable = (float *) fftwf_malloc ( ( ( samples / 2 ) + 1 ) * sizeof(float) );
		logtable[0] = -INFINITY;

		for ( unsigned i = 1 ; i < ( samples / 2 ) + 1 ; i++ )
			logtable[i] = log2f ( i );
	}

	fftwf_iodim dims = { samples , 1 , 1 };

	// Imaginary part of array must also be put somewhere starting on a 16byte boundary.
	// Hence up to 12 bytes between the arrays must be skipped.
	if ( ( fftplan = (fftwf_plan_s**) fftwf_plan_guru_split_dft_r2c ( 1 , & dims , 0 , 0 , buffer[0] , buffer[1] , buffer[1] + ( ( ( ( samples / 2 ) + 1 ) + 0x03 ) & ~0x03 ) , FFTW_ESTIMATE ) ) == NULL )
		qFatal ( "Couldn't build FFT plan." );

	if ( windowtype != None )
	{
		window = (float *) fftwf_malloc ( samples * sizeof(float) );

		switch ( windowtype )
		{
			case Triangle:
				BrianWindow::Triangle::generate ( window , samples );
				break;

			case Hann:
				BrianWindow::Hann::generate ( window , samples );
				break;

			case Hamming:
				BrianWindow::Hamming::generate ( window , samples );
				break;

			case FlatTop:
				BrianWindow::FlatTop::generate ( window , samples );
				break;

			case Sine:
				BrianWindow::Sine::generate ( window , samples );
				break;

			default:
				qFatal ( "Can't generate window type" );
		}
	}

//	qDebug () << "Built Painter" << this << "with s =" << samples;
}

BrianDataPainter::~BrianDataPainter ( void )
{
	fftwf_free ( buffer[0] );
	fftwf_free ( buffer[1] );

	if ( logtable )
		fftwf_free ( logtable );

	if ( window )
		fftwf_free ( window );

	fftwf_destroy_plan ( (fftwf_plan_s *) fftplan );

//	qDebug () << "Deleted Painter" << this << "with s =" << samples;
}

// Responsible for analyzing the raw jack data.
void BrianDataPainter::prepare ( float * data )
{
	// Unless something's changed, the buffer will still be in place from last time.
	if ( ( data != last_data ) || ( log_value != last_log_value ) || ( log_frequency != last_log_frequency ) || ( windowtype != last_windowtype ) )
	{	
		last_data = data;
		last_log_value = log_value;
		last_log_frequency = log_frequency;
		last_windowtype = windowtype;

		bool src = 0;
		float * source = data;
		float * dest = buffer[~src & 1];

		// Apply Window?
		if ( windowtype != None )
		{
			// Apply window.
			BrianMaths::mult_aligned_sf ( source , window , dest , samples );

			src^=1;
			source = buffer[src];
			dest = buffer[~src & 1];
		}

		// Do FFT.
		fftwf_execute_split_dft_r2c ( (const fftwf_plan) fftplan , source , dest , dest + ( ( ( ( samples / 2 ) + 1 ) + 0x03 ) & ~0x03 ) );

		src^=1;
		source = buffer[src];
		dest = buffer[~src & 1];

		// Get magnitude.
		BrianMaths::hypot_aligned_split_sf ( source , &source[( ( ( samples / 2 ) + 1 ) + 0x03 ) & ~0x03] , dest , (samples/2) + 1 );

		src^=1;
		source = buffer[src];
		dest = buffer[~src & 1];

		if ( log_value )
		{
			for ( unsigned i = 0 ; i < ( samples + 2 ) / 2 ; i++ )
				dest[i] = log2f ( source[i] );

			src^=1;
			source = buffer[src];
			dest = buffer[~src & 1];
		}

		// Interleave array for handing to vertexarray.
		if ( log_frequency )
		{
			for ( unsigned i = 0 , j = 0 ; i < samples + 2 ; i+=2 , j++ )
			{
				dest[i] = logtable[j];
				dest[i+1] = source[j];
			}

			src^=1;
			source = buffer[src];
			dest = buffer[~src & 1];
		}
		else
		{
			for ( unsigned i = 0 , j = 0 ; i < samples + 2 ; i+=2 , j++ )
			{
				dest[i] = j;
				dest[i+1] = source[j];
			}

			src^=1;
			source = buffer[src];
			dest = buffer[~src & 1];
		}

		last_prepared = buffer[src];
	}
}

// Responsible for dumping the float array onto the screen.
void BrianDataPainter::reduce ( unsigned samplerate_p )
{
	// Set up bin#/logbin# -> freq/logfreq matrix
	glMatrixMode ( GL_MODELVIEW );
		glLoadIdentity ();
		if ( last_log_frequency )
			glTranslatef ( log2f ( samplerate_p / (float) samples ) , 0.0 , 0.0 );
		else
			glScalef ( samplerate_p  / (float) samples , 1.0 , 1.0 );

		if ( last_log_value )
		{
			glScalef ( 1.0 , 20.0 / log2f ( 10.0 ) , 1.0 );
			glTranslatef ( 0.0 , 1.0 - log2f ( samples ) , 0.0 );
		}
		else
			// Normalise against analysis size
			glScalef ( 1.0 , 2.0 / samples , 1.0 );

	// Set pointer
	glVertexPointer ( 2 , GL_FLOAT , 0 , last_prepared );

	glEnableClientState ( GL_VERTEX_ARRAY );

	glPushAttrib ( GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_LINE_BIT );

		// QGLWidget::qglColor () aint static.
		glColor4f ( 0.0 , 0.0 , 0.0 , 1.0 );

		// We are going to carve an impression of the LINE_STRIP in the alpha,
		// then fill over it with colour later.
		glBlendEquation ( GL_FUNC_ADD );
		glBlendFunc ( GL_ONE , GL_ONE );

		glLineWidth ( 2.0 );

		// Draw data.
		glDrawArrays ( GL_LINE_STRIP , 0 , ( samples + 2 ) / 2 );

		glMatrixMode ( GL_PROJECTION );
		glPushMatrix ();
			glLoadIdentity ();
	
			glMatrixMode ( GL_MODELVIEW );
			glPushMatrix ();
				glLoadIdentity ();

				// Fill in the alpha'd portions of the framebuffer with the
				// appropriate colour.
				glBlendFunc ( GL_DST_ALPHA , GL_ONE );
				glColor4f ( colour.redF () , colour.greenF () , colour.blueF () , 1.0 );
	
				glRectf ( -1.0 , -1.0 , 1.0 , 1.0 );

				// Clear the alpha channel.
				glBlendEquation ( GL_FUNC_REVERSE_SUBTRACT );
				glBlendFunc ( GL_ONE , GL_ONE );
				glColor4f ( 0.0 , 0.0 , 0.0 , 1.0 );

				glRectf ( -1.0 , -1.0 , 1.0 , 1.0 );
	
			glPopMatrix ();
	
		glMatrixMode ( GL_PROJECTION );
		glPopMatrix ();

	glPopAttrib ();
}

void BrianDataPainter::changeColour ( const QColor & newcolour )
{
	colour = newcolour;
}

void BrianDataPainter::setWindowType ( enum BrianWindowType windowtype_p )
{
	if ( windowtype_p == windowtype )
		return;

	if ( windowtype == None )
		window = (float *) fftwf_malloc ( samples * sizeof(float) );

	if ( windowtype_p == None )
	{
		fftwf_free ( window );
		window = 0;
	}
	else
		switch ( windowtype_p )
		{
			case Triangle:
				BrianWindow::Triangle::generate ( window , samples );
				break;

			case Hann:
				BrianWindow::Hann::generate ( window , samples );
				break;

			case Hamming:
				BrianWindow::Hamming::generate ( window , samples );
				break;

			case FlatTop:
				BrianWindow::FlatTop::generate ( window , samples );
				break;

			case Sine:
				BrianWindow::Sine::generate ( window , samples );
				break;

			default:
				qFatal ( "Can't generate window type" );
		}

	windowtype = windowtype_p;
}

void BrianDataPainter::setLogValue ( bool log_value_p )
{
	log_value = log_value_p;
}

void BrianDataPainter::setLogFrequency ( bool log_frequency_p )
{
	if ( log_frequency == log_frequency_p )
		return;

	log_frequency = log_frequency_p;

	if ( log_frequency )
	{
		logtable = (float *) fftwf_malloc ( ( ( samples / 2 ) + 1 ) * sizeof(float) );
		logtable[0] = -INFINITY;

		for ( unsigned i = 1 ; i < ( samples / 2 ) + 1 ; i++ )
			logtable[i] = log2f ( i );
	}
	else
	{
		fftwf_free ( logtable );
		logtable = 0;
	}
}
