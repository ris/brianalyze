//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QtGui/QApplication>
#include <QtGui/QMainWindow>
#include <ctime>

#include "BrianJackConnection.h"

#include "BrianMainWindow.h"
#include "BrianScopeControl.h"

void showHelpAndQuit ( void )
{
	qDebug () << QObject::tr ( "Usage: brianalyze [--no-threaded-paint (deprecated)] [--paint-threading=(none|noreduce|full)] [--disable-stencil]" );
	exit ( 0 );
};

int main ( int argc , char *argv[] )
{
	QApplication brian_app ( argc , argv );

	QStringList arglist ( QApplication::arguments () );

	BrianJackConnection::PaintThreading threaded_paint = BrianJackConnection::NoReduce;
	bool stencil = true;
	foreach ( QString argument , arglist )
	{
		if ( argument == "--no-threaded-paint" )
			threaded_paint = BrianJackConnection::None;
		else if ( argument.startsWith ( "--paint-threading=" ) )
		{
			QString option ( argument.section ( '=' , 1 ) );

			if ( option == "none" )
				threaded_paint = BrianJackConnection::None;
			else if ( option == "noreduce" )
				threaded_paint = BrianJackConnection::NoReduce;
			else if ( option == "full" )
				threaded_paint = BrianJackConnection::Full;
			else
				showHelpAndQuit ();
		}
		else if ( argument == "--disable-stencil" )
			stencil = false;
		else if ( argument == "--help" )
		{
			showHelpAndQuit ();
		}
	}

	BrianMainWindow brianmainwindow ( 0 , threaded_paint , stencil );
	brianmainwindow.show ();

	return brian_app.exec ();
}


