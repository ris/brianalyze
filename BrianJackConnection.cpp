//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "BrianJackConnection.h"

#include "BrianDataSupplier.h"
#include "BrianDataPainter.h"

#include "BrianDefaults.h"
#include "BrianGarbage.h"

#include <sys/types.h>
#include <unistd.h>

#include <QtCore/QtDebug>
#include <QtCore/QByteArray>
#include <QtCore/QtAlgorithms>

#include <QtGui/QPainter>

#include <QtCore/QtConcurrentMap>


BrianJackConnection::BrianJackConnection ( PaintThreading threaded_paint_p ) : QObject ( 0 ) ,
	ports ( new BrianPortList ) ,
	samplehold ( false ) ,
	in_process ( 0 ) ,
	threaded_paint ( threaded_paint_p )
{
	BrianGarbage::add ( ports.pointer () );

	QByteArray clientname ( "brianalyze-" );

	clientname.append ( QByteArray::number ( getpid () ) );

	// Try to become a client of the JACK server
	if ( ( client = jack_client_new ( clientname ) ) == 0 )
		qFatal ( "Couldn't connect to jack server." );

	// Tell the JACK server to call jackProcess() whenever
	// there is work to be done.
	jack_set_process_callback ( client , BrianCallbacks::process , this );
	jack_set_buffer_size_callback ( client , BrianCallbacks::changeBlocksize , this );
	jack_set_sync_callback ( client , BrianCallbacks::transport , this );
	jack_on_shutdown ( client , BrianCallbacks::shutdown , this );

	// jack_set_port_connect_callback is quite new and not yet commonplace.
	// So for now, we're going to trigger on graph_order callbacks and make do with
	// the false positives.
//	jack_set_port_connect_callback ( client , BrianCallbacks::portConnect , this );
	jack_set_graph_order_callback ( client , BrianCallbacks::graphReorder , this );

	connect ( &relaythread , SIGNAL(dirtyTriggerReceived()) , this , SIGNAL(newData()) );

	relaythread.start ();
}

BrianJackConnection::~BrianJackConnection ( void )
{
	qDebug ( __PRETTY_FUNCTION__ );

	relaythread.safeKill ();
	// Should really wait for that to complete now.

	// delete all our ports.
	while ( ! (*ports).isEmpty () )
		delete ((*ports).takeFirst ()).pointer ();

	delete ports.pointer ();

	// Leave jack server
	jack_client_close ( client );
}

int BrianJackConnection::jackProcess ( jack_nframes_t nframes , void * data )
{
	in_process++;
	// ports pointer could get swapped out by UI thread while we're iterating.
	BrianPointer <BrianPortList> ports_snapshot ( ports );

	for ( BrianPortList::const_iterator i = ports_snapshot->constBegin () ; i != ports_snapshot->constEnd () ; i++ )
		(*i)->process ( samplehold );

	// mark brianscopewidget as dirty.
	if ( ! samplehold )
		relaythread.trigger ();

	in_process--;
	return 0;
}

int BrianJackConnection::jackChangeBlocksize ( jack_nframes_t nframes , void * data )
{
	return 0;
}

void BrianJackConnection::jackShutdown ( void * data )
{
	// TODO handle gracefully
	qDebug ( "Shutdown by jack server." );
	qDebug () << "in_process =" << in_process;
}

void BrianJackConnection::jackPortConnect ( void )
{
	emit portConnect ();
}

void BrianJackConnection::jackTransportStop ( void )
{
	emit transportStop ();
}

void BrianJackConnection::activate ( void )
{
	jack_activate ( client );
}

void BrianJackConnection::deactivate ( void )
{
	jack_deactivate ( client );
}

BrianDataManager * BrianJackConnection::addPort ( bool logfreq , bool logval )
{
	// Read, Copy...
	BrianPointer <BrianPortList> newlist ( new BrianPortList ( *ports ) );
	BrianGarbage::add ( newlist.pointer () );

	QByteArray namecandidate;

	// Find a unique name.
	unsigned i = 0;
	forever
	{
		bool matched = false;
		namecandidate = BrianDefaults::portname + QByteArray::number ( i );

		for ( BrianPortList::const_iterator iter = newlist->constBegin () ; iter != newlist->constEnd () ; iter++ )
		{
			if ( (*iter)->getPortName () == namecandidate )
			{
				matched = true;
				break;
			}
		}

		if ( ! matched )
			break;

		i++;
	}

	// Choose a colour for the new manager by finding the largest unoccupied
	// gap in the hue on the colour wheel and bisecting it.
	QList < int > huelist;

	// Get a sorted list of the occupied hues.
	for ( BrianPortList::const_iterator iter = newlist->constBegin () ; iter != newlist->constEnd () ; iter++ )
	{
		int hue = (*iter)->getColour().hue ();
		huelist.insert ( qLowerBound ( huelist.begin () , huelist.end () , hue ) , hue );
	}

	// Inspect all the gaps.
	QPair < int , int > chosen_gap ( 120 , 0 );
	for ( QList < int >::const_iterator iter = huelist.constBegin () ; iter != huelist.constEnd () ; iter++ )
	{
		QPair < int , int > candidate_gap;

		if ( iter+1 != huelist.constEnd () )
			candidate_gap = qMakePair ( *iter , *(iter+1) - *iter );
		else
			candidate_gap = qMakePair ( *iter , *huelist.constBegin () + 360 - *iter );

		if ( chosen_gap.second < candidate_gap.second )
			chosen_gap = candidate_gap;
	}

	//qDebug () << "chosen_gap =" << chosen_gap;

	BrianDataManager * newmanager = new BrianDataManager ( client , BrianDefaults::analysis_periods , BrianDefaults::windowtype , logfreq , logval , QColor::fromHsv ( ( chosen_gap.first + ( chosen_gap.second / 2 ) ) % 360 , 255 , 255 ) , namecandidate );
	newlist->append ( BrianPointer <BrianDataManager> ( newmanager ) );
	BrianGarbage::add ( newmanager );

	// connect signals & slots
	connect ( this , SIGNAL(samplerateChanged(unsigned)) , newmanager , SLOT(changeSamplerate(unsigned)) , Qt::DirectConnection );
	connect ( this , SIGNAL(blocksizeChanged(unsigned)) , newmanager , SLOT(changeBlocksize(unsigned)) , Qt::DirectConnection );

	// ...Update shared reference.
	ports = newlist;

	emit portListChanged ();

	return newmanager;
}

void BrianJackConnection::removePort ( BrianDataManager * manager_p )
{
	// Read, Copy...
	BrianPointer <BrianPortList> newlist ( new BrianPortList ( *ports ) );
	BrianGarbage::add ( newlist.pointer () );

	int oldtabindex = manager_p->tabIndex ();

	if ( ! newlist->removeOne ( BrianPointer <BrianDataManager> ( manager_p ) ) )
		qFatal ( "Can't remove port from port list" );

	for ( BrianPortList::const_iterator iter = newlist->constBegin () ; iter != newlist->constEnd () ; iter++ )
	{
		int tabindex = (*iter)->tabIndex ();
		if ( tabindex > oldtabindex )
			(*iter)->setTabIndex ( tabindex - 1 );
	}

	// ...Update shared reference.
	ports = newlist;

	emit portListChanged ();
}

// FIXME - this sort of stuff really doesn't belong in brianjackconnection.
void BrianJackConnection::paintAllConnections ( QPainter & painter_p )
{
	QPoint initial_offset ( 16 , painter_p.fontMetrics().height () + 1 );
	painter_p.translate ( initial_offset );

	QPoint zeropoint ( 0 , 0 );

	for ( BrianPortList::const_iterator iter = ports->constBegin () ; iter != ports->constEnd () ; iter++ )
	{
		if ( ! (*iter)->paintConnections ( painter_p ) )
		{
			painter_p.resetTransform ();
			painter_p.setPen ( Qt::white );
			painter_p.drawText ( 16 , painter_p.device()->height () , "..." );
			break;
		}

		QPoint line_offset ( 0 , painter_p.fontMetrics().height () + 1 );
		painter_p.translate ( line_offset );
	}
}

void BrianJackConnection::setLogFreq ( bool log )
{
	for ( BrianPortList::const_iterator iter = ports->constBegin () ; iter != ports->constEnd () ; iter++ )
	{
		(*iter)->changeLogFrequency ( log );
	}
}

void BrianJackConnection::setLogAmp ( bool log )
{
	for ( BrianPortList::const_iterator iter = ports->constBegin () ; iter != ports->constEnd () ; iter++ )
	{
		(*iter)->changeLogValue ( log );
	}
}

void BrianJackConnection::setSampleHold ( bool samplehold_p )
{
	samplehold = samplehold_p;
}

unsigned BrianJackConnection::getSamplerate ( void )
{
	return jack_get_sample_rate ( client );
}

// We need these wrappers because the qlist contains BrianPointers.
// We can't use member functions.
inline BrianDataManager * brian_map_wrapper ( const BrianPointer<BrianDataManager> & manager_p )
{
	manager_p->prepare ();
	return manager_p.pointer ();
}

inline void brian_reduce_wrapper ( int & reduced_p , BrianDataManager * manager_p )
{
	manager_p->reduce ();
	reduced_p++;
}

void BrianJackConnection::paintAll ( void )
{
	if ( threaded_paint == Full )
		QtConcurrent::blockingMappedReduced ( *ports , brian_map_wrapper , brian_reduce_wrapper );
	else
	{
		if ( threaded_paint == None )
		{
			for ( BrianPortList::const_iterator iter = ports->constBegin () ; iter != ports->constEnd () ; iter++ )
				(*iter)->prepare ();
		}
		else
		{
			QtConcurrent::blockingMap ( *ports , brian_map_wrapper );
		}
	
		for ( BrianPortList::const_iterator iter = ports->constBegin () ; iter != ports->constEnd () ; iter++ )
			(*iter)->reduce ();
	}
}

#include "BrianJackConnection.moc"
