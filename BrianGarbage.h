//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANGARBAGE_H
#define BRIANGARBAGE_H

#include <QtCore/QSharedData>

#include "BrianJackConnection.h"
#include "BrianPointer.h"

/**
** BrianGarbage:
**
** Rather than risk rt thread having to do a deletion, old objects with contention
** are added to the garbage pile.
**
** This keeps a reference to them and periodically deletes ones to which we are the
** only referrer.
**/
class BrianGarbage
{
	public:
		static void add ( BrianPortList * );
		static void add ( BrianDataManager * );
		static void add ( BrianDataPainter * );
		static void add ( BrianDataSupplier * );
		static void add ( BrianTransfer * );

		static void sweep ( void );

		static QSet < BrianPortList * > portlist_set;
		static QSet < BrianDataManager * > datamanager_set;
		static QSet < BrianDataPainter * > datapainter_set;
		static QSet < BrianDataSupplier * > datasupplier_set;
		static QSet < BrianTransfer * > transfer_set;
};

#endif /*BRIANGARBAGE_H*/
