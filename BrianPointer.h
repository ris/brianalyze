//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANPOINTER_H

#define BRIANPOINTER_H

#include <QtCore/QAtomicInt>
#include <QtCore/QDebug>

/** BrianPointer: a reference counting pointer with an (almost) atomic copy constructor.
**  Designed not to do any deletion itself. Instead leave that to a sweeping
**  process.
**/
template < typename T , bool nullable >
class BrianPointerBase
{
	private:
		typedef BrianPointerBase < T , nullable > ThisType;

	public:
		BrianPointerBase ( void ) : target ( 0 ) {};

		BrianPointerBase ( T * target_p ) : target ( target_p )
		{
			if ( (!nullable) || target != 0 )
				target->referrers.fetchAndAddOrdered ( 1 );

			//qDebug () << __PRETTY_FUNCTION__ << "target =" << target;
		};

		~BrianPointerBase ( void )
		{
			if ( (!nullable) || target != 0 )
				target->referrers.fetchAndAddOrdered ( -1 );

			//qDebug () << __PRETTY_FUNCTION__ << "target =" << target << "count was" << oldcount;
		};

		T * pointer ( void ) const
		{
			return target;
		};

		T * operator-> ( void ) const
		{
			return target;
		};

		T & operator* ( void )
		{
			return *target;
		};

		const T & operator* ( void ) const
		{
			return *target;
		};

		bool operator== ( const ThisType & other ) const
		{
			return ( target == other.pointer () );
		};

		bool operator== ( const T * other ) const
		{
			return ( target == other );
		};

	protected:
		ThisType & common_assignment ( const ThisType & other )
		{
			if ( this != &other )
			{
				if ( (!nullable) || target != 0 )
					target->referrers.fetchAndAddOrdered ( -1 );
		
				common_copy ( other );
			}
			//qDebug () << __PRETTY_FUNCTION__ << "old target =" << oldtarget << "count was" << oldtargetoldcount << ". new target =" << target << "count was" << newtargetoldcount;
			return *this;
		};

		void common_copy ( const volatile ThisType & other )
		{
			int count;
		
			forever
			{
				target = other.target;

				if ( nullable && __builtin_expect ( target == 0 , false ) )
					break;

				count = target->referrers;

				if ( __builtin_expect ( count == 0 , false ) )
					continue;

				if ( __builtin_expect ( target->referrers.testAndSetOrdered ( count , count+1 ) , true ) )
					break;
			}
		};

		T * target;

	private:
		// Shouldn't be used.
		ThisType & operator= ( const ThisType & ) {};
};

/** BrianPointer takes a shortcut by not checking for the pointer being null.
**/
template < typename T >
class BrianPointer : public BrianPointerBase < T , false >
{
	public:
		// Default (null) constructor is available but shouldn't be used unless
		// proper initialization is not possible. In such a case unsafeSet() can
		// be used to post-initialize it.
		BrianPointer ( void ) {};

		BrianPointer ( T * target_p ) : BrianPointerBase<T,false> ( target_p ) {};

		BrianPointer ( const BrianPointer<T> & other )
		{
			this->common_copy ( other );
		};

		// Horrible hack. Sometimes it is very inconvenient to have to initialize
		// a data member. This allows an otherwise un-nullable pointer to be initialized
		// to null and set in the constructor, without trying to dereference the null.
		void unsafeSet ( T * new_target )
		{
			BrianPointerBase<T,false>::target = new_target;

			BrianPointerBase<T,false>::target->referrers.fetchAndAddOrdered ( 1 );
		};

		BrianPointer<T> & operator= ( const BrianPointer<T> & other )
		{
			return static_cast<BrianPointer<T>&> ( BrianPointerBase<T,false>::common_assignment ( other ) );
		};
};

/** BrianNullablePointer does the null check on the pointer.
**/
template < typename T >
class BrianNullablePointer : public BrianPointerBase < T , true >
{
	public:
		BrianNullablePointer ( void ) {};

		BrianNullablePointer ( T * target_p ) : BrianPointerBase<T,true> ( target_p ) {};

		BrianNullablePointer ( const BrianNullablePointer<T> & other )
		{
			this->common_copy ( other );
		};

		BrianNullablePointer<T> & operator= ( const BrianNullablePointer<T> & other )
		{
			return static_cast<BrianNullablePointer<T>&> ( BrianPointerBase<T,true>::common_assignment ( other ) );
		};

		// Should be assignable from BrianPointers
		BrianNullablePointer<T> & operator= ( const BrianPointer<T> & other )
		{
			return static_cast<BrianNullablePointer<T>&> ( BrianPointerBase<T,true>::common_assignment ( (BrianNullablePointer<T>&) other ) );
		};

		operator bool ( void ) const
		{
			return ( BrianPointerBase<T,true>::target != 0 );
		};
};

class BrianPointerTarget
{
	public:
		BrianPointerTarget ( void ) : referrers ( 0 ) {};

		// Copies do not inherit references.
		BrianPointerTarget ( const BrianPointerTarget & ) : referrers ( 0 ) {};

		QAtomicInt referrers;

		// Shouldn't be used. Empty anyway. But QtConcurrent wants it I think.
		BrianPointerTarget & operator= ( const BrianPointerTarget & ) {};
};

#endif /*BRIANPOINTER_H*/
