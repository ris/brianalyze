//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANDATASUPPLIER_H

#define BRIANDATASUPPLIER_H

#include <QtCore/QByteArray>
#include <QtCore/QSharedData>

#include <jack/jack.h>

#include "BrianTransfer.h"
#include "BrianDataManager.h"
#include "BrianWindow.h"
#include "BrianPointer.h"

class BrianDataSupplier : public BrianPointerTarget
{
	public:
		BrianDataSupplier ( unsigned , unsigned , unsigned , BrianDataPainter * );

		~BrianDataSupplier ( void );

		// Doesn't pass/return BrianPointers. Calling function should be holding reference to
		// all relevant objects during call.
		BrianTransfer * process ( float * , BrianTransfer * , BrianTransfer * );

		void changeSamplerate ( unsigned );

	private:
		BrianPointer<BrianTransfer> transfer[3];

		unsigned samplerate;
		const unsigned blocksize;
		const unsigned analysis_periods;

		float * persistbuffer;
};

#endif /* BRIANDATASUPPLIER_H */
