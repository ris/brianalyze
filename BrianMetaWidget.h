//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANMETAWIDGET_H

#define BRIANMETAWIDGET_H

#include "BrianGridList.h"

#include "ui_brianmetawidget.h"

class BrianMetaWidget : public QWidget
{
	Q_OBJECT

	public:
		BrianMetaWidget ( QWidget * = 0 );
		~BrianMetaWidget ( void );

	public slots:
		void setLowFreq ( float );
		void setHighFreq ( float );

		void setLogFreq ( bool );

		void setLowAmp ( float );
		void setHighAmp ( float );

		void setLogAmp ( bool );

		void setUseStencil ( bool );

		void updateData ( void );
		void updateConnections ( void );

	signals:
		void paintSources ( void );
		void paintConnections ( QPainter & );

	private:
		Ui_BrianMetaWidget ui;

		BrianGridList * hgridlist;
		BrianGridList * vgridlist;
};

#endif /*BRIANMETAWIDGET_H*/
