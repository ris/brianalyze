//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANDEFAULTS_H

#define BRIANDEFAULTS_H

namespace BrianDefaults
{
	const float low_freq = 1.0;
	const float high_freq = 12000.0;

	const float low_val = 0.001;
	const float high_val = 1.1;

	const unsigned analysis_periods = 8;
	const enum BrianWindowType windowtype = Hann;

	const QByteArray portname ( "in" );
};

#endif /* BRIANDEFAULTS_H */
