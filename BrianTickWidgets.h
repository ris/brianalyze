//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANTICKWIDGETS_H

#define BRIANTICKWIDGETS_H

#include "BrianGridList.h"

#include <QtGui/QWidget>

class BrianAbstractTickWidget : public QWidget
{
	Q_OBJECT

	public:
		BrianAbstractTickWidget ( QWidget * = 0 );

		void setFont ( const QFont & , const QFontMetrics & );
		void setGridList ( const BrianGridList ** );

	protected:
		const BrianGridList ** gridlist;
};

class BrianHTickWidget : public BrianAbstractTickWidget
{
	Q_OBJECT

	public:
		BrianHTickWidget ( QWidget * = 0 );

	private:
		void paintEvent ( QPaintEvent * );
};

class BrianVTickWidget : public BrianAbstractTickWidget
{
	Q_OBJECT

	public:
		BrianVTickWidget ( QWidget * = 0 );

	private:
		void paintEvent ( QPaintEvent * );
};

#endif /*BRIANTICKWIDGETS_H*/
