//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianSlider.h"

BrianSlider::BrianSlider ( QWidget * parent , float multiplier_p ) : QSlider ( parent ) ,
	multiplier ( multiplier_p )
{
	connect ( this , SIGNAL(valueChanged(int)) , this , SLOT(valueChanged_relay(int)) );
}

void BrianSlider::setMinimum_transformed ( float minimum_p )
{
	if ( multiplier > 0.0 )
		setMinimum ( (int) ( minimum_p * multiplier ) );
	else
		setMaximum ( (int) ( minimum_p * multiplier ) );
}

void BrianSlider::setMaximum_transformed ( float maximum_p )
{
	if ( multiplier < 0.0 )
		setMinimum ( (int) ( maximum_p * multiplier ) );
	else
		setMaximum ( (int) ( maximum_p * multiplier ) );
}

float BrianSlider::getMinimum_transformed ( void )
{
	if ( multiplier > 0.0 )
		return minimum () / multiplier;
	else
		return maximum () / multiplier;
}

float BrianSlider::getMaximum_transformed ( void )
{
	if ( multiplier < 0.0 )
		return minimum () / multiplier;
	else
		return maximum () / multiplier;
}

float BrianSlider::value_transformed ( void )
{
	return value () / multiplier;
}

void BrianSlider::setValue_transformed ( float value_p )
{
	setValue ( (int) ( value_p * multiplier ) );
}

void BrianSlider::setMultiplier ( float multiplier_p )
{
	multiplier = multiplier_p;
}

void BrianSlider::multiplyMultiplier ( float multiplier_p )
{
	multiplier *= multiplier_p;
}

void BrianSlider::valueChanged_relay ( int value_p )
{
	emit valueChanged_transformed ( value_p / multiplier );
}

void BrianSlider::setTickInterval_transformed ( float interval_p )
{
	setTickInterval ( (int) qAbs ( ( interval_p * multiplier ) ) );
}

#include "BrianSlider.moc"
