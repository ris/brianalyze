//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianScopeWidget.h"

#include "BrianJackConnection.h"
#include "BrianDataPainter.h"
#include "BrianMaths.h"
#include "BrianDefaults.h"

#include <cmath>
#include <strings.h>

const quint8 BrianScopeWidget::stipplepattern[128] =
{
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0xcc , 0xcc , 0xcc , 0xcc ,
	0x33 , 0x33 , 0x33 , 0x33 ,
	0x33 , 0x33 , 0x33 , 0x33
};

BrianScopeWidget::BrianScopeWidget ( QWidget * parent ) : QGLWidget ( QGLFormat ( QGL::AlphaChannel | QGL::NoDepthBuffer | QGL::StencilBuffer ) , parent ) ,
	samplehold ( false ) ,
	has_shaders ( false ) ,
	low_freq ( BrianDefaults::low_freq ) ,
	high_freq ( BrianDefaults::high_freq ) ,
	low_val ( BrianDefaults::low_val ) ,
	high_val ( BrianDefaults::high_val ) ,
	log_frequency ( false ) ,
	log_value ( false ) ,
	freqgridlist ( 0 ) ,
	ampgridlist ( 0 ) ,
	use_stencil ( true )
{
}

BrianScopeWidget::~BrianScopeWidget ()
{

}

void BrianScopeWidget::setGridLists ( BrianGridList ** fgrid_p , BrianGridList ** agrid_p )
{
	freqgridlist = fgrid_p;
	ampgridlist = agrid_p;
}

void BrianScopeWidget::setLowFreq ( float freq )
{
	low_freq = freq;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setHighFreq ( float freq )
{
	high_freq = freq;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setLogFreq ( bool log )
{
	log_frequency = log;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setLowAmp ( float amp_p )
{
	low_val = amp_p;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setHighAmp ( float amp_p )
{
	high_val = amp_p;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setLogAmp ( bool log_p )
{
	log_value = log_p;

	setupProjectionMatrix ();
	generateGrid ();

	update ();
}

void BrianScopeWidget::setUseStencil ( bool s_p )
{
	use_stencil = s_p;
};

void BrianScopeWidget::initializeGL ( void )
{
	if ( ! ( format().openGLVersionFlags () & QGLFormat::OpenGL_Version_1_2  ) )
		qDebug ( "You have a GL implementation < 1.2. Some things may not work properly." );

	// Don't need any extensions yet.
//	QByteArray extensions ( (const char *) glGetString ( GL_EXTENSIONS ) );
//	 qDebug () << "GL extensions: " << extensions;

// 	if ( extensions.contains ( "GL_ARB_vertex_shader" ) && extensions.contains ( "GL_ARB_shading_language_100" ) )
// 		has_shaders = true;
// 
// 	if ( extensions.contains ( "GL_ARB_vertex_buffer_object" ) )
// 		has_vbo = true;

	glEnable ( GL_LINE_SMOOTH );
	glHint ( GL_LINE_SMOOTH_HINT , GL_NICEST );

	glDisable ( GL_DEPTH_TEST );
	glDisable ( GL_LIGHTING );

	glEnable ( GL_STENCIL_TEST );
	glClearStencil ( 0 );
	glClear ( GL_STENCIL_BUFFER_BIT );

	glClearColor ( 0.0 , 0.0 , 0.0 , 0.0 );

	glEnable ( GL_BLEND );

	glPolygonStipple ( stipplepattern );

	glStencilFunc ( GL_EQUAL , 0x00 , 0xff );
	glStencilOp ( GL_KEEP , GL_KEEP , GL_KEEP );
}

void BrianScopeWidget::resizeGL ( int width , int height )
{
	glViewport ( 0 , 0 , width , height );

	setupProjectionMatrix ();

	// Regen grid.
	generateGrid ();
}

void BrianScopeWidget::constructStencil ( void )
{
	glClear ( GL_STENCIL_BUFFER_BIT );

	glMatrixMode ( GL_MODELVIEW );
	glPushMatrix ();
		glLoadIdentity ();

		glMatrixMode ( GL_PROJECTION );
		glPushMatrix ();
			glLoadIdentity ();

			glEnable ( GL_STENCIL_TEST );

				glPushAttrib ( GL_STENCIL_BUFFER_BIT );

					glStencilFunc ( GL_NEVER , 0x01 , 0xff );
					glStencilOp ( GL_REPLACE , GL_KEEP , GL_KEEP );
		
					glEnable ( GL_POLYGON_STIPPLE );
	
						glRectf ( -1.0 , -1.0 , 1.0 , 1.0 );
		
					glDisable ( GL_POLYGON_STIPPLE );
	
					glStencilOp ( GL_ZERO , GL_KEEP , GL_KEEP );
	
					glOrtho ( 0.0 , width () , height () , 0.0 , -1.0 , 1.0 );
					glRecti ( 8 , 8 , width () - 8 , height () - 7 );

				glPopAttrib ();

			glDisable ( GL_STENCIL_TEST );

		glPopMatrix ();

	glMatrixMode ( GL_MODELVIEW );
	glPopMatrix ();
}

void BrianScopeWidget::paintGL ( void )
{
	glClear ( GL_COLOR_BUFFER_BIT );

	// Must regenerate stencil every time because we cant guarantee
	// window will be in same place on the framebuffer every time.
	// This is to do with the way DRI works and may not be an issue on
	// other platforms.
	constructStencil ();

	if ( use_stencil )
		glEnable ( GL_STENCIL_TEST );

		// Draw data
		emit paintSources ();

	glDisable ( GL_STENCIL_TEST );

	// Draw gridlines
	Q_ASSERT ( freqgridlist );
	Q_ASSERT ( ampgridlist );
	Q_ASSERT ( *freqgridlist );
	Q_ASSERT ( *ampgridlist );

	(*freqgridlist)->drawLines ( false );
	(*ampgridlist)->drawLines ( true );
}

void BrianScopeWidget::generateGrid ( void )
{
	// Check pointers and pointed-to pointers are non-null
	if ( ! ( freqgridlist && ampgridlist ) )
		return;

	if ( *freqgridlist )
		delete *freqgridlist;

	if ( *ampgridlist )
		delete *ampgridlist;

	float low_amp_limit , high_amp_limit;

	// Need to be careful with plotting decibels linearly because of asymptote at 0
	// causing infinite gridlines. Therefore impose sane limit.
	if ( ! log_value )
		low_amp_limit = BrianMaths::toDecibels ( low_val + ( ( high_val - low_val ) * 0.05 ) );
	else
		low_amp_limit = BrianMaths::toDecibels ( low_val );

	high_amp_limit = BrianMaths::toDecibels ( high_val );

	// Generate new gridlists.
	if ( ! log_frequency )
		*freqgridlist = new BrianGridList ( *this , (unsigned) ( width () / 40.0 ) , low_freq , high_freq , 10000.0 , 0x1861861 , false , false , false );
	else
		*freqgridlist = new BrianGridList ( *this , (unsigned) ( width () / 40.0 ) , low_freq , high_freq , 10000.0 , 0x30c30c , true , true , false );

	*ampgridlist = new BrianGridList ( *this , (unsigned) ( height () / 40.0 ) , low_amp_limit , high_amp_limit , 24.0 , 0x1251861 , false , log_value ? 0 : -1 , true );
}

void BrianScopeWidget::setupProjectionMatrix ( void )
{
	Q_ASSERT ( low_freq < high_freq );
	Q_ASSERT ( low_val < high_val );

	// Set up projection matrix for freq/logfreq -> graph
	glMatrixMode ( GL_PROJECTION );
	glLoadIdentity ();

	// Scale from beyond stencilled area
	glScalef ( 1.0 - ( 16.0 / width () ) , 1.0 - ( 16.0 / height () ) , 1.0 );

	// off-centre.
	glTranslatef ( -1.0 , -1.0 , 0.0 );

	if ( log_frequency )
	{
		// pre-PROJECTION units will be log2(Hz)
		Q_ASSERT ( low_freq > 0 );
		glScalef ( 2.0 / log2f ( high_freq / low_freq ) , 1.0 , 1.0 );
		glTranslatef ( -log2f ( low_freq ) , 0.0 , 0.0 );
	}
	else
	{
		// pre-PROJECTION units will be Hz
		Q_ASSERT ( low_freq >= 0 );
		glScalef ( 2.0 / ( high_freq - low_freq ) , 1.0 , 1.0 );
		glTranslatef ( -low_freq , 0.0 , 0.0 );
	}

	if ( log_value )
	{
		// pre-PROJECTION units will be decibels
		Q_ASSERT ( true ); // ?
		glScalef ( 1.0 , 2.0 / BrianMaths::toDecibels ( high_val / low_val ) , 1.0 );
		glTranslatef ( 0.0 , -BrianMaths::toDecibels ( low_val ) , 0.0 );
	}
	else
	{
		// pre-PROJECTION units will be normalised FFT magnitude
		Q_ASSERT ( true ); // ?
		glScalef ( 1.0 , 2.0 / ( high_val - low_val ) , 1.0 );
		glTranslatef ( 0.0 , -low_val , 0.0 );
	}

}

void BrianScopeWidget::hideEvent ( QHideEvent * event )
{
	setUpdatesEnabled ( false );
}

void BrianScopeWidget::showEvent ( QShowEvent * event )
{
	setUpdatesEnabled ( true );
}

#include "BrianScopeWidget.moc"
