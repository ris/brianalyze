//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianCallbacks.h"

int BrianCallbacks::process ( jack_nframes_t frames , void * connection )
{
	// Beware unsafe cast.
	static_cast<BrianJackConnection *>(connection)->jackProcess ( frames , connection );
}

int BrianCallbacks::changeBlocksize ( jack_nframes_t frames , void * connection )
{
	
}

void BrianCallbacks::shutdown ( void * connection )
{
	static_cast<BrianJackConnection *>(connection)->jackShutdown ( 0 );
}

int BrianCallbacks::transport ( jack_transport_state_t state_p , jack_position_t * position_p , void * connection_p )
{
// 	if ( state_p == JackTransportStopped )
// 		static_cast<BrianJackConnection *>(connection_p)->jackTransportStop ();

	qDebug () << "Received transport state" << (int)state_p;

	// We are always 'ready'.
	return 1;
}

void BrianCallbacks::portConnect ( jack_port_id_t port_a_p , jack_port_id_t port_b_p , int n , void * connection )
{
	// Beware unsafe cast.
	static_cast<BrianJackConnection *>(connection)->jackPortConnect ();
}

// TODO remove once jack_set_port_connect_callback becomes more widespread.
int BrianCallbacks::graphReorder ( void * connection )
{
	static_cast<BrianJackConnection *>(connection)->jackPortConnect ();
	return 0;
}
