//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANDATAPAINTER_H

#define BRIANDATAPAINTER_H

#include <QtGui/QColor>
#include <QtCore/QSharedData>
#include <fftw3.h>

#include "BrianWindow.h"
#include "BrianPointer.h"


class BrianDataPainter : public BrianPointerTarget
{
	public:
		BrianDataPainter ( unsigned , enum BrianWindowType , bool , bool , const QColor & );

		~BrianDataPainter ( void );

		void prepare ( float * );
		void reduce ( unsigned );

		void changeColour ( const QColor & );
		void setWindowType ( enum BrianWindowType );
		void setLogFrequency ( bool );
		void setLogValue ( bool );

	private:
		bool log_frequency , log_value;
		bool last_log_frequency , last_log_value;

		const unsigned samples;

		QColor colour;

		fftwf_plan * fftplan;

		// Ping-pong buffers.
		float * buffer[2];

		float * logtable;

		enum BrianWindowType windowtype;
		enum BrianWindowType last_windowtype;
		float * window;

		const float * last_data;
		const float * last_prepared;
};

#endif /* BRIANDATAPAINTER_H */
