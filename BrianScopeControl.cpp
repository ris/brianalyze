//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianScopeControl.h"

#include <cmath>

BrianScopeControl::BrianScopeControl ( QWidget * parent ) : QWidget ( parent ) ,
	margin ( 1.0 ) ,
	realminswappedout ( false )
{
	ui.setupUi ( this );

	ui.high_slider->setMultiplier ( -1.0 );

	// TODO
	//ui.low_spinbox->setMinimum ( 1.0 );
	//ui.low_slider->setMinimum_transformed ( 1.0 );
	//ui.high_spinbox->setMinimum ( 2.0 );
	//ui.high_slider->setMinimum_transformed ( 2.0 );

	//ui.high_spinbox->setMaximum ( 10.0 );
	//ui.high_slider->setMaximum_transformed ( 10.0 );
	//ui.low_spinbox->setMaximum ( 9.0 );
	//ui.low_slider->setMaximum_transformed ( 9.0 );

	connect ( ui.low_slider , SIGNAL(valueChanged_transformed(float)) , this , SLOT(lowChanged_private(float)) );
	connect ( ui.high_slider , SIGNAL(valueChanged_transformed(float)) , this , SLOT(highChanged_private(float)) );
	connect ( ui.low_spinbox , SIGNAL(valueChanged(double)) , this , SLOT(lowSpinboxChanged(double)) );
	connect ( ui.high_spinbox , SIGNAL(valueChanged(double)) , this , SLOT(highSpinboxChanged(double)) );

	connect ( ui.log_check , SIGNAL(toggled(bool)) , this , SLOT(logDisallowZero(bool)) );

	//ui.low_spinbox->setValue ( 1.0 );
	//ui.high_spinbox->setValue ( 9.0 );
}

void BrianScopeControl::lowSpinboxChanged ( double val_p )
{
	lowChanged_private ( (float) val_p );
}

void BrianScopeControl::lowChanged_private ( float low_p )
{
	if ( ui.low_spinbox->value () != low_p )
		ui.low_spinbox->setValue ( low_p );
	else if ( ui.low_slider->value_transformed () != low_p )
		ui.low_slider->setValue_transformed ( low_p );
	else
		return;

	ui.high_slider->setMinimum_transformed ( low_p + margin );
	ui.high_spinbox->setMinimum ( low_p + margin );

	emit lowChanged ( low_p );
}

void BrianScopeControl::highSpinboxChanged ( double val_p )
{
	highChanged_private ( (float) val_p );
}

void BrianScopeControl::highChanged_private ( float high_p )
{
	if ( ui.high_spinbox->value () != high_p )
		ui.high_spinbox->setValue ( high_p );
	else if ( ui.high_slider->value_transformed () != high_p )
		ui.high_slider->setValue_transformed ( high_p );
	else
		return;

	ui.low_slider->setMaximum_transformed ( high_p - margin );
	ui.low_spinbox->setMaximum ( high_p - margin );

	emit highChanged ( high_p );
}

bool BrianScopeControl::isLog ( void ) const
{
	return ui.log_check->isChecked ();
}

float BrianScopeControl::low ( void ) const
{
	return ui.low_spinbox->value ();
}

float BrianScopeControl::high ( void ) const
{
	return ui.high_spinbox->value ();
}

void BrianScopeControl::setMinimum ( float min_p )
{
	ui.low_spinbox->setMinimum ( min_p );
	ui.low_slider->setMinimum_transformed ( min_p );

	if ( ui.high_slider->getMinimum_transformed () < min_p + margin )
	{
		ui.high_slider->setMinimum_transformed ( min_p + margin );
		ui.high_spinbox->setMinimum ( min_p + margin );
	}
}

void BrianScopeControl::setMaximum ( float max_p )
{
	ui.high_spinbox->setMaximum ( max_p );
	ui.high_slider->setMaximum_transformed ( max_p );

	if ( ui.low_slider->getMaximum_transformed () > max_p - margin )
	{
		ui.low_slider->setMaximum_transformed ( max_p - margin );
		ui.low_spinbox->setMaximum ( max_p - margin );
	}
}

void BrianScopeControl::setSuffix ( const QString & suffix_p )
{
	ui.low_spinbox->setSuffix ( suffix_p );
	ui.high_spinbox->setSuffix ( suffix_p );
}

void BrianScopeControl::setTickInterval ( float interval_p )
{
	ui.low_slider->setTickInterval_transformed ( interval_p );
	ui.high_slider->setTickInterval_transformed ( interval_p );
}

void BrianScopeControl::setMargin ( float margin_p )
{
	margin = margin_p;
	ui.low_spinbox->setSingleStep ( margin );
	ui.high_spinbox->setSingleStep ( margin );

	unsigned decimals = (unsigned) qAbs ( std::floor ( std::log10 ( margin_p ) ) );

	ui.low_spinbox->setDecimals ( decimals );
	ui.high_spinbox->setDecimals ( decimals );
}

void BrianScopeControl::reset ( void )
{
	ui.low_spinbox->setValue ( ui.low_spinbox->minimum () );
	ui.high_spinbox->setValue ( ui.high_spinbox->maximum () );
}

//
// This is all horribly ugly, but if you ask me, ui code is always ugly.
// So it doesn't matter.
//
void BrianScopeControl::logDisallowZero ( bool log_p )
{
	if ( log_p )
	{
		if ( ui.low_spinbox->minimum () <= margin )
		{
			realmin = ui.low_spinbox->minimum ();
			realminswappedout = true;
			setMinimum ( margin );
		}
	}
	else
	{
		if ( realminswappedout )
		{
			realminswappedout = false;
			setMinimum ( realmin );
		}
	}

	emit logChanged ( log_p );
}

void BrianScopeControl::multiplyMultiplier ( float mul_p )
{
	ui.low_slider->multiplyMultiplier ( mul_p );
	ui.high_slider->multiplyMultiplier ( mul_p );
}

#include "BrianScopeControl.moc"
