//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANMAINWINDOW_H

#define BRIANMAINWINDOW_H

#include <QtGui/QPushButton>

#include "ui_main.h"
#include "BrianJackConnection.h"

class BrianMainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		BrianMainWindow ( QWidget * , BrianJackConnection::PaintThreading , bool );
		~BrianMainWindow ( void );

	signals:
		void tabChanged ( int );

	public slots:
		void setTabIcon ( unsigned , const QIcon & );
		void setTabText ( unsigned , const QByteArray & );
		void addPort ( void );
		void removePort ( void );

	private slots:
		void disableEnableRemove ( int );
		void garbageCall ( void );

	private:
		Ui_BrianMainWindow ui;
		QToolButton * addbutton;
		QToolButton * removebutton;

		QTimer garbagetimer;
		BrianJackConnection jackconnection;
};

#endif /* BRIANMAINWINDOW_H */
