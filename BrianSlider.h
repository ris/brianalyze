//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANSLIDER_H

#define BRIANSLIDER_H

#include <QtGui/QSlider>

class BrianSlider : public QSlider
{
	Q_OBJECT
	
	public:
		BrianSlider ( QWidget * = 0 , float = 1.0 );

		float value_transformed ( void );
		float getMinimum_transformed ( void );
		float getMaximum_transformed ( void );

	public slots:
		void setMinimum_transformed ( float );
		void setMaximum_transformed ( float );

		void setValue_transformed ( float );

		void setTickInterval_transformed ( float );

		void setMultiplier ( float );
		void multiplyMultiplier ( float );

	signals:
		void valueChanged_transformed ( float );

	private slots:
		void valueChanged_relay ( int );

	private:
		float multiplier;
};

#endif /* BRIANFREQSLIDER_H */
