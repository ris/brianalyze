//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianBlinkButton.h"

#include <QtCore/QTimerEvent>

BrianBlinkButton::BrianBlinkButton ( QWidget * parent = 0 ) :
	QPushButton ( parent ) ,
	lit_palette ( QColor ( 255 , 0 , 0 ) ) ,
	is_lit ( false )
{
	connect ( this , SIGNAL(toggled(bool)) , this , SLOT(changeState(bool)) );
}

void BrianBlinkButton::timerEvent ( QTimerEvent * event )
{
	if ( event->timerId () != blink_timer_id )
		return;

	if ( is_lit )
	{
		setPalette ( parentWidget()->palette() );
		is_lit = false;
	} else
	{
		setPalette ( lit_palette );
		is_lit = true;
	}
}

void BrianBlinkButton::changeState ( bool checked )
{
	if ( checked )
		blink_timer_id = startTimer ( 500 );
	else
	{
		killTimer ( blink_timer_id );
		if ( is_lit )
		{
			setPalette ( parentWidget()->palette () );
			is_lit = false;
		}
	}
}

void BrianBlinkButton::check ( void )
{
	setChecked ( true );
}

#include "BrianBlinkButton.moc"
