//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANDATAMANAGER_H

#define BRIANDATAMANAGER_H

#include <QtCore/QByteArray>
#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <QtCore/QSharedData>
#include <jack/jack.h>

#include "BrianDataSupplier.h"
#include "BrianDataPainter.h"
#include "BrianTransfer.h"
#include "BrianWindow.h"
#include "BrianPointer.h"

#include "ui_managercontrol.h"

class BrianDataSupplier;
class BrianDataPainter;

/** BrianDataManager:
**
** The only persistent representation of a channel.
** Coordinates behaviour of painters and suppliers.
**
**/
class BrianDataManager : public QWidget , public BrianPointerTarget
{
	Q_OBJECT

	public:
		BrianDataManager ( jack_client_t * , unsigned , enum BrianWindowType , bool , bool , QColor , const QByteArray & );
		~BrianDataManager ();

		void process ( bool );
		void prepare ( void );
		void reduce ( void );
		bool paintConnections ( QPainter & );

		const QByteArray & getPortName ( void ) const;
		const QIcon & getIcon ( void ) const;
		const QColor & getColour ( void ) const;

		void setTabIndex ( unsigned );
		int tabIndex ( void ) const;

	public slots:
		// Called from jack thread
		void changeBlocksize ( unsigned );
		void changeSamplerate ( unsigned );

		// Called from Qt thread
		void changeAnalysisPeriods ( int );
		void changeWindowType ( int );
		void changePortName ( void );
		void changeLogFrequency ( bool );
		void changeLogValue ( bool );
		void changeColour ( const QColor & );

		void colourClicked ( void );

	signals:
		void iconChanged ( unsigned , const QIcon & );
		void nameChanged ( unsigned , const QByteArray & );

	private:
		void setUiColour ( void );

		BrianNullablePointer<BrianTransfer> latest_complete;
		BrianNullablePointer<BrianTransfer> reading_from;

		BrianPointer<BrianDataSupplier> datasupplier;
		BrianPointer<BrianDataPainter> datapainter;

		QMutex supplier_rebuild;
		QMutex painter_rebuild;

		jack_client_t *const jackclient;
		jack_port_t * jackport;

		unsigned blocksize;
		unsigned samplerate;
		unsigned analysisperiods;
		enum BrianWindowType windowtype;
		bool log_freq;
		bool log_val;
		QColor colour;

		QIcon colouricon;

		QByteArray portname;

		unsigned tabindex;

		Ui_managercontrol ui;
};

#endif /* BRIANDATAMANAGER_H */
