//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BrianMetaWidget.h"

BrianMetaWidget::BrianMetaWidget ( QWidget * parent_p ) : QWidget ( parent_p ) ,
	hgridlist ( 0 ) ,
	vgridlist ( 0 )
{
	QFont tempfont ( "Fixed" , 8 );
	tempfont.setFixedPitch ( true );
	setFont ( tempfont );

	QPalette temppalette;
	temppalette.setColor ( QPalette::Window , Qt::black );
	setPalette ( temppalette );

	// text aa against dark backgrounds is often ugly
	// because of incorrect gamma settings.
	tempfont.setStyleStrategy ( QFont::PreferBitmap );

	ui.setupUi ( this );

	ui.scopewidget->setGridLists ( &hgridlist , &vgridlist );

	// Why is this cast necessary? It makes no sense.
	ui.htick->setGridList ( (const BrianGridList**) &hgridlist );
	ui.vtick->setGridList ( (const BrianGridList**) &vgridlist );

	connect ( ui.scopewidget , SIGNAL(paintSources(void)) , this , SIGNAL(paintSources(void)) , Qt::DirectConnection );
	connect ( ui.legendwidget , SIGNAL(paintConnections(QPainter&)) , this , SIGNAL(paintConnections(QPainter&)) , Qt::DirectConnection );
}

BrianMetaWidget::~BrianMetaWidget ( void )
{
	delete hgridlist;
	delete vgridlist;
}

void BrianMetaWidget::setLowFreq ( float freq_p )
{
	ui.scopewidget->setLowFreq ( freq_p );

	ui.htick->update ();
}

void BrianMetaWidget::setHighFreq ( float freq_p )
{
	ui.scopewidget->setHighFreq ( freq_p );

	ui.htick->update ();
}

void BrianMetaWidget::setLogFreq ( bool log_p )
{
	ui.scopewidget->setLogFreq ( log_p );

	ui.htick->update ();
}

void BrianMetaWidget::setLowAmp ( float amp_p )
{
	ui.scopewidget->setLowAmp ( amp_p );

	ui.vtick->update ();
}

void BrianMetaWidget::setHighAmp ( float amp_p )
{
	ui.scopewidget->setHighAmp ( amp_p );

	ui.vtick->update ();
}

void BrianMetaWidget::setLogAmp ( bool log_p )
{
	ui.scopewidget->setLogAmp ( log_p );

	ui.vtick->update ();
}

void BrianMetaWidget::setUseStencil ( bool s_p )
{
	ui.scopewidget->setUseStencil ( s_p );
	ui.scopewidget->update ();
}

void BrianMetaWidget::updateData ( void )
{
	ui.scopewidget->update ();
}

void BrianMetaWidget::updateConnections ( void )
{
	ui.legendwidget->update ();
}

#include "BrianMetaWidget.moc"
