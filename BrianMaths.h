//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BRIANMATHS_H

#define BRIANMATHS_H

#include <cmath>

typedef float v4sf __attribute__ ((vector_size (16)));

namespace BrianMaths
{
	template < class T >
	inline T toDecibels ( T n )
	{
		return 20.0 * log10 ( n );
	};

	template < class T >
	inline T fromDecibels ( T n )
	{
		return exp10 ( n / 20.0 );
	};

	inline void hypot_aligned_split_sf ( const float * in_a , const float * in_b , float * out , unsigned n )
	{
		unsigned i = 0;

		for ( ; i + 3 < n ; i += 4 )
		{
			v4sf a = *((v4sf*) & in_a[i]);
			v4sf b = *((v4sf*) & in_b[i]);

			a *= a;
			b *= b;

			a += b;
#ifdef __SSE__
			a = __builtin_ia32_sqrtps ( a );
#else
			float * f = (float*) &a;

			f[0] = sqrt ( f[0] );
			f[1] = sqrt ( f[1] );
			f[2] = sqrt ( f[2] );
			f[3] = sqrt ( f[3] );
#endif

			v4sf * out_vector = (v4sf*) & out[i];
			(*out_vector) = a;
		}

		for ( ; i < n ; i++ )
			out[i] = hypot ( in_a[i] , in_b[i] );
	};

	inline void mult_aligned_sf ( const float * in_a , const float * in_b , float * out , unsigned n )
	{
		unsigned i = 0;

		for ( ; i + 3 < n ; i += 4 )
		{
			v4sf a = *((v4sf*) & in_a[i]);
			v4sf b = *((v4sf*) & in_b[i]);

			v4sf * out_vector = (v4sf*) & out[i];
			(*out_vector) = a * b;
		}

		for ( ; i < n ; i++ )
			out[i] = in_a[i] * in_b[i];
	}
};

#endif /*BRIANMATHS_H*/
