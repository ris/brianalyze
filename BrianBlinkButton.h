//     Brianalyze - A simple JACK-connected spectrum analyzer.
//     Copyright (C) 2008 Robert Scott, code at humanleg dot org dot uk
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QtGui/QPushButton>

#ifndef BRIANBLINKBUTTON_H

#define BRIANBLINKBUTTON_H

class BrianBlinkButton : public QPushButton
{
	Q_OBJECT

	public:
		BrianBlinkButton( QWidget * parent );

	public slots:
		void check ( void );

	private slots:
		void changeState ( bool checked );

	private:
		void timerEvent ( QTimerEvent * event );

		const QPalette lit_palette;
		bool is_lit;
		int blink_timer_id;
};

#endif /* BRIANBLINKBUTTON_H */
